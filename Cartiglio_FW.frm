VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form Cartiglio_FW 
   Caption         =   "Cartiglio Firmware"
   ClientHeight    =   10455
   ClientLeft      =   870
   ClientTop       =   765
   ClientWidth     =   13095
   LinkTopic       =   "Form2"
   ScaleHeight     =   10455
   ScaleWidth      =   13095
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command3 
      Caption         =   "Open folder"
      Height          =   360
      Left            =   4245
      TabIndex        =   15
      Top             =   2400
      Width           =   2595
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Open PDF"
      Height          =   330
      Left            =   4230
      TabIndex        =   14
      Top             =   2790
      Visible         =   0   'False
      Width           =   2610
   End
   Begin MSDataGridLib.DataGrid DataGrid2 
      Height          =   2940
      Left            =   7500
      TabIndex        =   13
      Top             =   765
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   5186
      _Version        =   393216
      Appearance      =   0
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         RecordSelectors =   0   'False
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Height          =   5850
      Left            =   225
      TabIndex        =   12
      Top             =   3825
      Width           =   12585
      _ExtentX        =   22199
      _ExtentY        =   10319
      _Version        =   393216
      Appearance      =   0
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         Locked          =   -1  'True
         RecordSelectors =   0   'False
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Data Data4 
      Appearance      =   0  'Flat
      Caption         =   "Data4"
      Connect         =   "Access 2000;"
      DatabaseName    =   ""
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   9255
      Options         =   0
      ReadOnly        =   -1  'True
      RecordsetType   =   1  'Dynaset
      RecordSource    =   ""
      Top             =   9930
      Visible         =   0   'False
      Width           =   2412
   End
   Begin VB.Data Data3 
      Appearance      =   0  'Flat
      Caption         =   "Data3"
      Connect         =   "Access 2000;"
      DatabaseName    =   ""
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   6615
      Options         =   0
      ReadOnly        =   -1  'True
      RecordsetType   =   1  'Dynaset
      RecordSource    =   ""
      Top             =   9915
      Visible         =   0   'False
      Width           =   2412
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   600
      Left            =   5490
      TabIndex        =   6
      Top             =   210
      Visible         =   0   'False
      Width           =   1050
   End
   Begin VB.Data Data2 
      Caption         =   "Data2"
      Connect         =   "Access 2000;"
      DatabaseName    =   ""
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   3885
      Options         =   0
      ReadOnly        =   -1  'True
      RecordsetType   =   1  'Dynaset
      RecordSource    =   ""
      Top             =   9945
      Visible         =   0   'False
      Width           =   2412
   End
   Begin VB.Data Data1 
      Caption         =   "Data1"
      Connect         =   "Access 2000;"
      DatabaseName    =   ""
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   1305
      Options         =   0
      ReadOnly        =   -1  'True
      RecordsetType   =   1  'Dynaset
      RecordSource    =   ""
      Top             =   9990
      Visible         =   0   'False
      Width           =   2412
   End
   Begin VB.ComboBox Combo1 
      DataField       =   "firmware"
      DataMember      =   "cod_fw"
      DataSource      =   "Data1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Cartiglio_FW.frx":0000
      Left            =   225
      List            =   "Cartiglio_FW.frx":0002
      TabIndex        =   5
      Top             =   765
      Width           =   7170
   End
   Begin VB.Label Label11 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Sales code"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   225
      TabIndex        =   17
      Top             =   1425
      Width           =   1380
   End
   Begin VB.Label Label10 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      DataSource      =   "Data2"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   300
      Left            =   225
      TabIndex        =   16
      Top             =   1695
      Width           =   1680
   End
   Begin VB.Label Label9 
      BackStyle       =   0  'Transparent
      Caption         =   "Associated modules"
      Height          =   255
      Left            =   7500
      TabIndex        =   11
      Top             =   525
      Width           =   2385
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   300
      Left            =   2025
      TabIndex        =   10
      Top             =   2415
      Width           =   1245
   End
   Begin VB.Label Label8 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   300
      Left            =   210
      TabIndex        =   9
      Top             =   2400
      Width           =   1485
   End
   Begin VB.Label Label7 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   300
      Left            =   210
      TabIndex        =   8
      Top             =   3120
      Width           =   7170
   End
   Begin VB.Label Label6 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      DataSource      =   "Data2"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   300
      Left            =   2055
      TabIndex        =   7
      Top             =   1695
      Width           =   1680
   End
   Begin VB.Label Label5 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Release"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   2025
      TabIndex        =   4
      Top             =   2130
      Width           =   1860
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Version"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   210
      TabIndex        =   3
      Top             =   2130
      Width           =   1575
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Internal name"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   210
      TabIndex        =   2
      Top             =   2850
      Width           =   1695
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Firmware type"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   2040
      TabIndex        =   1
      Top             =   1425
      Width           =   1740
   End
   Begin VB.Label Cod 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Firmware Code"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   225
      TabIndex        =   0
      Top             =   495
      Width           =   1590
   End
End
Attribute VB_Name = "Cartiglio_FW"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim db1 As ADODB.Connection
Dim rs1 As ADODB.Recordset
Dim rs2 As ADODB.Recordset
Dim rs3 As ADODB.Recordset

Private Sub Combo1_Change()

carica_dati

End Sub

Private Sub Combo1_Click()
carica_dati
End Sub

Private Sub Command2_Click()

    perc_rdm = Read_Set("Server", "Cartiglio_fw_path")
    perc_rdm = Left(perc_rdm, Len(perc_rdm) - 1)
    sFile = perc_rdm & Combo1.Text & "\" & Combo1.Text & "_list.pdf"
    lRet = ShellExecute(0, "open", sFile, "", "", 1)

End Sub

Private Sub Command3_Click()
On Error GoTo error
 perc_rdm = Read_Set("Server", "Cartiglio_fw_path")
    perc_rdm = Left(perc_rdm, Len(perc_rdm) - 1)
    sFile = perc_rdm & "\" & Combo1.Text & "\" '& Combo1.Text & "_list.pdf"
    lRet = ShellExecute(0, "explore", sFile, "", "", 1)
Exit Sub

error:

MsgBox "Cartella non trovata", vbOKOnly

End Sub

Private Sub Form_Activate()

If Disposizione_cartiglio_fw = False Then

disposizione_ricerca = True
For Each obj In Cartiglio_FW

    On Error Resume Next
    obj.Height = obj.Height * Scala
    obj.Width = obj.Width * Scala
    obj.Top = obj.Top * Scala
    obj.Left = obj.Left * Scala
    obj.Font = "Arial"



Next
MSFlexGrid1.Width = Cartiglio_FW.Width - 600
Disposizione_cartiglio_fw = True
dataGrid1.HeadFont.Size = 10
dataGrid1.HeadFont.Bold = True

Set db1 = New ADODB.Connection
Set rs1 = New ADODB.Recordset
Set rs2 = New ADODB.Recordset
Set rs3 = New ADODB.Recordset

rs1.CursorLocation = adUseClient
rs2.CursorLocation = adUseClient
rs3.CursorLocation = adUseClient



aa = Read_Set("Server", "databasePath")
aa = Left(aa, Len(aa) - 1)

db1.Open "Provider =Microsoft.Jet.OLEDB.4.0;Data Source=" & aa & ";"
End If

rs1.Close
rs1.Open ("SELECT cod_fw as Cod_FW, nome_intero as Nome_Interno from firmware order by cod_fw"), db1, adOpenStatic, adLockReadOnly
rs1.Requery
'Data1.Recordset.MoveLast
'Data1.Recordset.MoveFirst

Do While Not rs1.EOF
    Combo1.AddItem rs1.Fields("cod_fw") '& "   " & rs1.Fields("Nome_intero")
    rs1.MoveNext
Loop

Combo1.Text = Fw_Cartiglio

End Sub

Private Sub Form_Load()
Cartiglio_FW.Caption = "Cartiglio firmware " & Fw_Cartiglio

End Sub
Sub carica_dati()
On Error Resume Next
Cartiglio_FW.Caption = "Cartiglio firmware " & Combo1.Text
rs1.Close
rs1.Open ("SELECT * from firmware where cod_fw = '" & Combo1.Text & "' order by cod_fw"), db1, adOpenStatic, adLockReadOnly
rs1.Requery
If rs1.RecordCount > 0 Then
    
    aa = rs1.Fields("Tipo_fw")
    rs2.Close
    kk = "select Descrizione from Tipo_fw where tipo_fw =" & Str(aa)
    rs2.Open ("select Tipo_fw.Descrizione from Tipo_fw where tipo_fw.tipo_fw =" & Str(aa)), db1, adOpenStatic, adLockReadOnly
    rs2.Requery
    Label6.Caption = rs2.Fields("descrizione")
    Label7.Caption = rs1.Fields("nome_intero")
    Label10.Caption = rs1.Fields("cod_vendita")
    dataGrid1.Width = Cartiglio_FW.Width - 600
    
    If aa = 2 Then
        Label4.Caption = "Tipo CDF"
        Label8.Caption = rs1.Fields("Tipo_cdf")
        Label5.Visible = False
        Label1.Visible = False
        rs2.Close
        rs2.Open ("select iso_code, Versione_iso, fitness from dettaglio_rif where cod_fw = '" & Combo1.Text & "'"), db1, adOpenStatic, adLockReadOnly
        rs2.Requery
        Set dataGrid1.DataSource = rs2
        dataGrid1.Columns.Item(0).Width = dataGrid1.Width * 14 / 100
        dataGrid1.Columns.Item(1).Width = dataGrid1.Width * 41 / 100
        dataGrid1.Columns.Item(2).Width = dataGrid1.Width * 41 / 100
        
    Else
        Label4.Caption = "Version"
        Label8.Caption = rs1.Fields("Versione")
        Label5.Visible = True
        Label1.Visible = True
        
        Label1.Caption = rs1.Fields("Release")
        rs2.Close
        rs2.Open ("select Nome_int as Nome_Interno, descrizione_ass As Descrizione from tbldett_fw where cod_fw = '" & Combo1.Text & "'"), db1, adOpenStatic, adLockReadOnly
        rs2.Requery
        Set dataGrid1.DataSource = rs2
        dataGrid1.Columns.Item(0).Width = dataGrid1.Width * 48 / 100
        dataGrid1.Columns.Item(1).Width = dataGrid1.Width * 49 / 100
        
    End If '
    rs3.Close
    rs3.Open ("select fw_mod.Id_modulo, moduli.descrizione from fw_mod, moduli where fw_mod.cod_fw = '" & Combo1.Text & "' And fw_mod.id_modulo = Moduli.id_modulo"), db1, adOpenStatic, adLockReadOnly
    rs3.Requery
    Set DataGrid2.DataSource = rs3
    
    DataGrid2.Width = Cartiglio_FW.Width - (600 + DataGrid2.Left - dataGrid1.Left)
    DataGrid2.Columns.Item(0).Width = DataGrid2.Width * 19 / 100
    DataGrid2.Columns.Item(1).Width = DataGrid2.Width * 76 / 100

    bb = bb
Else
    Label4.Caption = "Version"
    Label8.Caption = ""
    Label6.Caption = ""
    Label7.Caption = ""
    Label1.Caption = ""
    Label5.Visible = True
    Label1.Visible = True
End If
End Sub

Private Sub Form_Terminate()
Unload Cartiglio_FW
End Sub

Private Sub Form_Unload(Cancel As Integer)
Unload Cartiglio_FW
End Sub
