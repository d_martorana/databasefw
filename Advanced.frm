VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form Advanced 
   Caption         =   "Ricerca avanzata"
   ClientHeight    =   9945
   ClientLeft      =   165
   ClientTop       =   855
   ClientWidth     =   14085
   LinkTopic       =   "Form2"
   ScaleHeight     =   9945
   ScaleWidth      =   14085
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.PictureBox Picture1 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      Height          =   8580
      Left            =   75
      Picture         =   "Advanced.frx":0000
      ScaleHeight     =   8520
      ScaleWidth      =   15360
      TabIndex        =   5
      Top             =   2850
      Visible         =   0   'False
      Width           =   15420
   End
   Begin VB.CommandButton Command2 
      Caption         =   "DB structure"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   700
      Left            =   9735
      TabIndex        =   3
      Top             =   525
      Width           =   2000
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Search"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   700
      Left            =   9705
      TabIndex        =   1
      Top             =   1575
      Width           =   2000
   End
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1770
      Left            =   2850
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   525
      Width           =   8235
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHFlexGrid1 
      CausesValidation=   0   'False
      Height          =   5295
      Left            =   90
      TabIndex        =   4
      Top             =   2850
      Width           =   12360
      _ExtentX        =   21802
      _ExtentY        =   9340
      _Version        =   393216
      Cols            =   1
      FixedCols       =   0
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLines       =   2
      GridLinesUnpopulated=   2
      MergeCells      =   4
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   1
   End
   Begin VB.Image Image3 
      Height          =   630
      Left            =   4950
      Picture         =   "Advanced.frx":1CAF9
      Top             =   240
      Width           =   2325
   End
   Begin VB.Label Label1 
      Caption         =   "Insert the SQL string"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   630
      TabIndex        =   2
      Top             =   210
      Width           =   4320
   End
   Begin VB.Menu file 
      Caption         =   "File"
      Begin VB.Menu excel 
         Caption         =   "Save as Excel format"
      End
   End
End
Attribute VB_Name = "Advanced"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
On Error Resume Next
    rs1.Close
    rs1.Open (Text1.Text), db1, adOpenStatic, adLockReadOnly
    rs1.Requery
    aa = rs1.RecordCount
    Set MSHFlexGrid1.DataSource = rs1
    Picture1.Visible = False
    MSHFlexGrid1.Visible = True
End Sub

Private Sub Command2_Click()

Picture1.Visible = True
MSHFlexGrid1.Visible = False

End Sub

Private Sub excel_Click()
    Dim cn As ADODB.Connection
    Set cn = New ADODB.Connection
    
    Ricerca.salva1.InitDir = "Documenti"
    Ricerca.salva1.Filter = "Excel file |*.xls"
    Ricerca.salva1.Flags = cdlOFNOverwritePrompt Or cdlOFNPathMustExist
    Ricerca.salva1.Filename = ""
    Ricerca.salva1.ShowSave
    DoEvents
    
    If Ricerca.salva1.Filename <> "" Then
        File_excel = Ricerca.salva1.Filename
    Else
        Exit Sub
    End If

    If Dir(File_excel) <> "" Then 'elimino il file se esiste altrimenti va in errore
    
        Kill File_excel
    
    End If
    
    cn.ConnectionString = db1.ConnectionString
    cn.Open
    Dim strSQL As String
    For a = 1 To Len(rs1.Source)
        If UCase(Mid$(rs1.Source, a, 4)) = "FROM" Then
            aa = Right$(rs1.Source, (Len(rs1.Source) - a) + 2)
            bb = UCase(Mid$(rs1.Source, a, 4))
            strSQL = Mid$(rs1.Source, 1, a - 1) & "INTO [Excel 8.0;Database=" & File_excel & "].[Foglio1]" & Right$(rs1.Source, (Len(rs1.Source) - a) + 2)
            Exit For
        End If
    Next
    

    
   cn.Execute strSQL
    cn.Close

End Sub

Private Sub Form_Activate()
If disposizione_advanced = False Then
disposizione_advanced = True
    On Error Resume Next
    For Each obj In Advanced
    
       
        'obj.Visible = False
        obj.Height = obj.Height * Scala
        obj.Width = obj.Width * Scala
        obj.Top = obj.Top * Scala
        obj.Left = obj.Left * Scala
        obj.Font = "Arial"
    Next
    MSHFlexGrid1.Left = 100
    MSHFlexGrid1.Height = Screen.Height - MSHFlexGrid1.Top - 1000
    MSHFlexGrid1.Width = Screen.Width - 400
    MSHFlexGrid1.FontBand(0).Bold = True
    MSHFlexGrid1.Font.Size = 9
    MSHFlexGrid1.Font.Bold = False
    Picture1.AutoSize = False
    Picture1.AutoSize = True
    Image3.Width = 2325
    Image3.Height = 630
    Image3.Left = Screen.Width - 400 - Image3.Width
    Text1.Left = Label1.Left
End If

End Sub

Private Sub Form_Load()
    aa = Read_Set("Server", "databasePath")
    aa = Left(aa, Len(aa) - 1)
    
    Set db1 = New ADODB.Connection

    Set rs1 = New ADODB.Recordset

    db1.Open "Provider =Microsoft.Jet.OLEDB.4.0;Data Source=" & aa & ";"

    
    rs1.CursorLocation = adUseClient


End Sub

Private Sub Form_Terminate()
disposizione_advanced = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
disposizione_advanced = False
End Sub
