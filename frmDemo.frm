VERSION 5.00
Begin VB.Form frmPRINT 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Stampa"
   ClientHeight    =   9120
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   12555
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   9120
   ScaleWidth      =   12555
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox List1 
      Height          =   840
      Left            =   315
      TabIndex        =   8
      Top             =   555
      Width           =   5370
   End
   Begin VB.CheckBox chkColWidth 
      Caption         =   "Automatic columns resizing"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   330
      TabIndex        =   7
      Top             =   8565
      Value           =   1  'Checked
      Width           =   3570
   End
   Begin VB.PictureBox picScroll 
      Height          =   6345
      Left            =   255
      ScaleHeight     =   6285
      ScaleWidth      =   11895
      TabIndex        =   3
      Top             =   1995
      Width           =   11955
      Begin VB.VScrollBar vscScroll 
         Height          =   5955
         LargeChange     =   15
         Left            =   11640
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   -15
         Width           =   255
      End
      Begin VB.HScrollBar hscScroll 
         Height          =   255
         LargeChange     =   15
         Left            =   150
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   5970
         Width           =   11460
      End
      Begin VB.PictureBox picTarget 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   5940
         Left            =   0
         ScaleHeight     =   5910
         ScaleWidth      =   11565
         TabIndex        =   6
         Top             =   0
         Width           =   11595
      End
   End
   Begin VB.CommandButton cmdRefresh 
      Caption         =   "Preview refresh"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   9660
      TabIndex        =   2
      Top             =   8520
      Width           =   2580
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Print"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1425
      Left            =   9435
      TabIndex        =   1
      Top             =   225
      Width           =   2655
   End
   Begin VB.Label Label1 
      Caption         =   "Print preview"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Index           =   1
      Left            =   255
      TabIndex        =   9
      Top             =   1575
      Width           =   4740
   End
   Begin VB.Label Label1 
      Caption         =   "Select the printer"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Index           =   0
      Left            =   315
      TabIndex        =   0
      Top             =   135
      Width           =   4740
   End
End
Attribute VB_Name = "frmPRINT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'The dimensions of the DIN A4 paper size in Twips:
Const A4Height = 16840, A4Width = 11907

'To get the scroll width:
Private Declare Function GetSystemMetrics Lib "user32" (ByVal nIndex As Long) As Long
Private Const SM_CYHSCROLL = 3
Private Const SM_CXVSCROLL = 2

'Declared Private WithEvents to get NewPage event:
Private WithEvents cTP As clsTablePrint
Attribute cTP.VB_VarHelpID = -1
Private Sub FillFlexGrid()
'    Dim lCol As Long, lRow As Long
'    With fxgSrc
'        .Rows = 12
'        .ColWidth(0) = 150
'        For lCol = 0 To .Cols - 1
'            .ColWidth(lCol) = .ColWidth(lCol) * 2
'            For lRow = .FixedRows To .Rows - 1
'                If lCol = 0 Then
'                    .TextMatrix(lRow, 0) = "Row " & CStr(lRow)
'                Else
'                    .TextMatrix(lRow, lCol) = "Row " & CStr(lRow) & ", Col " & CStr(lCol)
'                End If
'            Next
'        Next
'        .Col = 0
'        .Row = 0
'        .FillStyle = flexFillRepeat
'        .ColSel = .Cols - 1
'        .CellFontBold = True
'        .FillStyle = flexFillSingle
'        .Col = 1
'        .Row = 1
'    End With
End Sub

Private Sub InitializePictureBox()
    Dim sngVSCWidth As Single, sngHSCHeight As Single
    'Set the size to the DIN A4 width:
    picTarget.Width = A4Height
    picTarget.Height = A4Width
    'Resize the scrollbars:
    sngVSCWidth = GetSystemMetrics(SM_CXVSCROLL) * Screen.TwipsPerPixelX
    sngHSCHeight = GetSystemMetrics(SM_CYHSCROLL) * Screen.TwipsPerPixelY
    hscScroll.Move 0, picScroll.ScaleHeight - sngHSCHeight, picScroll.ScaleWidth - sngVSCWidth, sngHSCHeight
    vscScroll.Move picScroll.ScaleWidth - sngVSCWidth, 0, sngVSCWidth, picScroll.ScaleHeight
    
    SetScrollBars
End Sub

Private Sub SetScrollBars()
    hscScroll.Max = (picTarget.Width - picScroll.ScaleWidth + vscScroll.Width) / 120 + 1
    vscScroll.Max = (picTarget.Height - picScroll.ScaleHeight + hscScroll.Height) / 120 + 1
End Sub


Private Sub cmdPrint_Click()
    
    'If MsgBox("The application will now print the grid on the default printer (Show a print dialog here later !).", vbInformation + vbOKCancel, "Print") = vbCancel Then Exit Sub
    Printer.Orientation = 2
    'Simply initialize the printer:
    Printer.Print
    
    'Read the FlexGrid:
    'Set the wanted width of the table to -1 to get the exact widths of the FlexGrid,
    ' to ScaleWidth - [the left and right margins] to get a fitting table !
    ImportFlexGrid cTP, Griglia_da_stampare, IIf((chkColWidth.Value = vbChecked), Printer.ScaleWidth - 2 * 567, -1)
    
    'Set margins (not needed, but looks better !):
    cTP.MarginBottom = 567 '567 equals to 1 cm
    cTP.MarginLeft = 567
    cTP.MarginTop = 567
    
    'Class begins drawing at CurrentY !
    Printer.CurrentY = cTP.MarginTop
    
    'Finally draw the Grid !
    cTP.DrawTable Printer
    'Done with drawing !
    
    'Say VB it should finally send it:
    Printer.EndDoc
End Sub

Private Sub cmdRefresh_Click()
    
    'Read the FlexGrid:
    'Set the wanted width of the table to -1 to get the exact widths of the FlexGrid,
    ' to ScaleWidth - [the left and right margins] to get a fitting table !
    ImportFlexGrid cTP, Griglia_da_stampare, IIf((chkColWidth.Value = vbChecked), picTarget.ScaleWidth - 2 * 567, -1)
    
    'Set margins (not needed, but looks better !):
    cTP.MarginBottom = 567 '567 equals to 1 cm
    cTP.MarginLeft = 567
    cTP.MarginTop = 567
    
    'Clear the box:
    picTarget.Cls
    
    'Class begins drawing at CurrentY !
    picTarget.CurrentY = cTP.MarginTop
    
    'Finally draw the Grid !
    cTP.DrawTable picTarget
    'Done with drawing !
End Sub

Private Sub Command1_Click()

End Sub

Private Sub cTP_NewPage(objOutput As Object, TopMarginAlreadySet As Boolean, bCancel As Boolean, ByVal lLastPrintedRow As Long)
    
    'The class wants a new page, look what to do
    If TypeOf objOutput Is Printer Then
        Printer.NewPage
    Else 'We are printing on the PictureBox !
        objOutput.CurrentY = objOutput.ScaleHeight
        'Simply increase the height of the PicBox here
        ' (very simple, but looks bad in "real" applications)
        objOutput.Height = objOutput.Height + A4Height
        'Draw a line to show the new page:
        objOutput.Line (0, objOutput.CurrentY)-(objOutput.ScaleWidth, objOutput.CurrentY), &H808080
        
        'Set the CurrentY to the position the class should continie with drawing and...
        objOutput.CurrentY = objOutput.CurrentY + cTP.MarginTop
        '... tell it to do so:
        TopMarginAlreadySet = True
        
        'Set the ScrollBar Max properties:
        SetScrollBars
    End If
End Sub

Private Sub Form_Load()
On Error Resume Next
Dim a As Integer
For Each X In Printers

List1.AddItem X.DeviceName

If Printer.DeviceName = "" Then
    cmdPrint.Enabled = False
    cmdRefresh.Enabled = False
End If

For a = 0 To List1.ListCount - 1
    If Printer.DeviceName = List1.List(a) Then
        List1.Selected(a) = True
    End If
Next


Next
    InitializePictureBox
    FillFlexGrid
    Set cTP = New clsTablePrint
    cmdRefresh_Click
End Sub


Private Sub hscScroll_Change()
    picTarget.Left = -hscScroll.Value * 120
End Sub

Private Sub hscScroll_Scroll()
    hscScroll_Change
End Sub


Private Sub List1_Click()
Dim aa As String
aa = List1.List(List1.ListIndex)
For Each X In Printers
    If X.DeviceName = aa Then
        Set Printer = X
        Exit For
    End If
Next
Printer.Orientation = 2
cmdRefresh.Enabled = True
cmdPrint.Enabled = True
End Sub

Private Sub vscScroll_Change()
    Dim aa As Long
    aa = -vscScroll.Value * 12
    picTarget.Top = aa * 10
End Sub


Private Sub vscScroll_Scroll()
    vscScroll_Change
End Sub


