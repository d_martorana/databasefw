VERSION 5.00
Begin VB.Form frmCDF 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "RICERCA CDF"
   ClientHeight    =   5835
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   12705
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5835
   ScaleWidth      =   12705
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frameReader 
      Caption         =   "Reader type"
      Height          =   4095
      Left            =   3720
      TabIndex        =   32
      Top             =   840
      Width           =   1215
      Begin VB.OptionButton chkReader 
         Caption         =   "RS32J"
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   42
         Top             =   3600
         Width           =   855
      End
      Begin VB.OptionButton chkReader 
         Caption         =   "RS32H"
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   41
         Top             =   3240
         Width           =   855
      End
      Begin VB.OptionButton chkReader 
         Caption         =   "RS32G"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   40
         Top             =   2880
         Width           =   855
      End
      Begin VB.OptionButton chkReader 
         Caption         =   "RS32F"
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   39
         Top             =   2520
         Width           =   855
      End
      Begin VB.OptionButton chkReader 
         Caption         =   "RS25"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   38
         Top             =   2160
         Width           =   855
      End
      Begin VB.OptionButton chkReader 
         Caption         =   "RS22"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   37
         Top             =   1800
         Width           =   855
      End
      Begin VB.OptionButton chkReader 
         Caption         =   "RS15"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   36
         Top             =   1440
         Width           =   855
      End
      Begin VB.OptionButton chkReader 
         Caption         =   "RS12"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   35
         Top             =   1080
         Width           =   855
      End
      Begin VB.OptionButton chkReader 
         Caption         =   "RS00"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   34
         Top             =   720
         Width           =   855
      End
      Begin VB.OptionButton chkReader 
         Caption         =   "All"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   33
         Top             =   360
         Value           =   -1  'True
         Width           =   855
      End
   End
   Begin VB.CommandButton btnChiudi 
      Cancel          =   -1  'True
      Caption         =   "CLOSE"
      Height          =   615
      Left            =   8040
      TabIndex        =   31
      Top             =   5040
      Width           =   4455
   End
   Begin VB.Frame Frame3 
      Caption         =   "Filter by selected currencies"
      Height          =   4095
      Left            =   240
      TabIndex        =   27
      Top             =   840
      Width           =   3375
      Begin VB.CheckBox chkValuta 
         Caption         =   "Currency 1"
         Enabled         =   0   'False
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   28
         Top             =   360
         Value           =   1  'Checked
         Width           =   1215
      End
      Begin VB.CheckBox chkValuta 
         Caption         =   "Currency 2"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   1
         Top             =   720
         Width           =   1215
      End
      Begin VB.CheckBox chkValuta 
         Caption         =   "Currency 3"
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   3
         Top             =   1080
         Width           =   1215
      End
      Begin VB.CheckBox chkValuta 
         Caption         =   "Currency 4"
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   5
         Top             =   1440
         Width           =   1215
      End
      Begin VB.CheckBox chkValuta 
         Caption         =   "Currency 5"
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   7
         Top             =   1800
         Width           =   1215
      End
      Begin VB.CheckBox chkValuta 
         Caption         =   "Currency 6"
         Height          =   255
         Index           =   5
         Left            =   240
         TabIndex        =   9
         Top             =   2160
         Width           =   1215
      End
      Begin VB.CheckBox chkValuta 
         Caption         =   "Currency 7"
         Height          =   255
         Index           =   6
         Left            =   240
         TabIndex        =   11
         Top             =   2520
         Width           =   1215
      End
      Begin VB.CheckBox chkValuta 
         Caption         =   "Currency 8"
         Height          =   255
         Index           =   7
         Left            =   240
         TabIndex        =   13
         Top             =   2880
         Width           =   1215
      End
      Begin VB.CheckBox chkValuta 
         Caption         =   "Currency 9"
         Height          =   255
         Index           =   8
         Left            =   240
         TabIndex        =   15
         Top             =   3240
         Width           =   1215
      End
      Begin VB.CheckBox chkValuta 
         Caption         =   "Currency 10"
         Height          =   255
         Index           =   9
         Left            =   240
         TabIndex        =   17
         Top             =   3600
         Width           =   1215
      End
      Begin VB.ComboBox cboValuta 
         Height          =   315
         Index           =   0
         ItemData        =   "frmCDF.frx":0000
         Left            =   1440
         List            =   "frmCDF.frx":0002
         TabIndex        =   0
         Top             =   360
         Width           =   1815
      End
      Begin VB.ComboBox cboValuta 
         Enabled         =   0   'False
         Height          =   315
         Index           =   1
         Left            =   1440
         TabIndex        =   2
         Top             =   720
         Width           =   1815
      End
      Begin VB.ComboBox cboValuta 
         Enabled         =   0   'False
         Height          =   315
         Index           =   2
         Left            =   1440
         TabIndex        =   4
         Top             =   1080
         Width           =   1815
      End
      Begin VB.ComboBox cboValuta 
         Enabled         =   0   'False
         Height          =   315
         Index           =   3
         Left            =   1440
         TabIndex        =   6
         Top             =   1440
         Width           =   1815
      End
      Begin VB.ComboBox cboValuta 
         Enabled         =   0   'False
         Height          =   315
         Index           =   4
         Left            =   1440
         TabIndex        =   8
         Top             =   1800
         Width           =   1815
      End
      Begin VB.ComboBox cboValuta 
         Enabled         =   0   'False
         Height          =   315
         Index           =   5
         Left            =   1440
         TabIndex        =   10
         Top             =   2160
         Width           =   1815
      End
      Begin VB.ComboBox cboValuta 
         Enabled         =   0   'False
         Height          =   315
         Index           =   6
         Left            =   1440
         TabIndex        =   12
         Top             =   2520
         Width           =   1815
      End
      Begin VB.ComboBox cboValuta 
         Enabled         =   0   'False
         Height          =   315
         Index           =   7
         Left            =   1440
         TabIndex        =   14
         Top             =   2880
         Width           =   1815
      End
      Begin VB.ComboBox cboValuta 
         Enabled         =   0   'False
         Height          =   315
         Index           =   8
         Left            =   1440
         TabIndex        =   16
         Top             =   3240
         Width           =   1815
      End
      Begin VB.ComboBox cboValuta 
         Enabled         =   0   'False
         Height          =   315
         Index           =   9
         Left            =   1440
         TabIndex        =   18
         Top             =   3600
         Width           =   1815
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "FF code found"
      Height          =   4095
      Left            =   5160
      TabIndex        =   26
      Top             =   840
      Width           =   2655
      Begin VB.ListBox lstFW 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3435
         Left            =   240
         TabIndex        =   19
         Top             =   480
         Width           =   2175
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Currencies"
      Height          =   4095
      Left            =   8040
      TabIndex        =   22
      Top             =   840
      Width           =   4455
      Begin VB.ListBox lstValute 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3435
         ItemData        =   "frmCDF.frx":0004
         Left            =   240
         List            =   "frmCDF.frx":0006
         TabIndex        =   20
         Top             =   480
         Width           =   3975
      End
      Begin VB.Label Label4 
         Caption         =   "FITNESS"
         Height          =   255
         Left            =   1920
         TabIndex        =   25
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "VER"
         Height          =   255
         Left            =   1200
         TabIndex        =   24
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "ISO CODE"
         Height          =   255
         Left            =   240
         TabIndex        =   23
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.CommandButton btnCerca 
      Caption         =   "SEARCH"
      Default         =   -1  'True
      Height          =   615
      Left            =   240
      TabIndex        =   30
      Top             =   5040
      Width           =   4815
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Caption         =   "SELECT THE CODE TO SEE THE COMPOSITION"
      Height          =   615
      Left            =   5160
      TabIndex        =   29
      Top             =   5040
      Width           =   2655
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "SEARCH CDF BY CURRENCY"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   21
      Top             =   120
      Width           =   12255
   End
End
Attribute VB_Name = "frmCDF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim myDataBase As ADODB.Connection
    Dim myRecordSet As ADODB.Recordset
    Dim databaseFileName As String
Private Sub btnCerca_Click()
    On Error Resume Next
    Dim i, numCampi As Integer
    Dim trovato As Boolean
    Set myDataBase = New ADODB.Connection
    Set myRecordSet = New ADODB.Recordset
    Dim myRecordSetModuli, myRecordSetRisultato As Recordset
    Set myRecordSetModuli = New ADODB.Recordset
    Set myRecordSetRisultato = New ADODB.Recordset
    Dim Ricerca(10) As String
    trovato = False
    'stringaRicerca = "'" & cboValuta(0).Text & "'"
    numCampi = 1
    For i = 1 To 9
        If cboValuta(i).Text <> "" Then
            'stringaRicerca = stringaRicerca & ", '" & cboValuta(i).Text & "'" 'AND table2.iso_code='USD'
            numCampi = numCampi + 1
        End If
    Next
    For i = 0 To UBound(Ricerca) - 1
        Ricerca(i) = cboValuta(i).Text
    Next
    myDataBase.Open "Provider =Microsoft.Jet.OLEDB.4.0;Data Source=" & databaseFileName & ";"
    Select Case numCampi
        Case 1
            myRecordSet.Open ("SELECT DISTINCT cod_fw FROM dettaglio_rif WHERE iso_code = '" & Ricerca(0) & "'"), myDataBase, adOpenStatic, adLockReadOnly
        Case 2
            myRecordSet.Open ("SELECT DISTINCT dettaglio_rif.cod_fw FROM dettaglio_rif INNER JOIN dettaglio_rif AS tabella2 ON Dettaglio_rif.cod_fw = tabella2.cod_fw WHERE " & _
            "(Dettaglio_rif.iso_code = '" & Ricerca(0) & "' AND tabella2.iso_code = '" & Ricerca(1) & "')"), myDataBase, adOpenStatic, adLockReadOnly
        Case 3
            myRecordSet.Open ("SELECT DISTINCT dettaglio_rif.cod_fw FROM (dettaglio_rif INNER JOIN dettaglio_rif AS tabella2 ON Dettaglio_rif.cod_fw = tabella2.cod_fw)" & _
            "INNER JOIN Dettaglio_rif AS tabella3 ON tabella2.Cod_fw = tabella3.Cod_fw WHERE (Dettaglio_rif.iso_code = '" & Ricerca(0) & "' AND tabella2.iso_code = '" & _
            Ricerca(1) & "' AND tabella3.iso_code = '" & Ricerca(2) & "')"), myDataBase, adOpenStatic, adLockReadOnly
        Case 4
            myRecordSet.Open ("SELECT DISTINCT dettaglio_rif.cod_fw FROM ((dettaglio_rif INNER JOIN dettaglio_rif AS tabella2 ON Dettaglio_rif.cod_fw = tabella2.cod_fw) " & _
            "INNER JOIN Dettaglio_rif AS tabella3 ON tabella2.Cod_fw = tabella3.Cod_fw) INNER JOIN Dettaglio_rif AS tabella4 ON tabella3.Cod_fw = tabella4.Cod_fw WHERE " & _
            "(Dettaglio_rif.iso_code = '" & Ricerca(0) & "' AND tabella2.iso_code = '" & Ricerca(1) & "' AND tabella3.iso_code = '" & Ricerca(2) & "' AND tabella4.iso_code = '" & _
            Ricerca(3) & "')"), myDataBase, adOpenStatic, adLockReadOnly
        Case 5
            myRecordSet.Open ("SELECT DISTINCT dettaglio_rif.cod_fw FROM (((dettaglio_rif INNER JOIN dettaglio_rif AS tabella2 ON Dettaglio_rif.cod_fw = tabella2.cod_fw) " & _
            "INNER JOIN Dettaglio_rif AS tabella3 ON tabella2.Cod_fw = tabella3.Cod_fw) INNER JOIN Dettaglio_rif AS tabella4 ON tabella3.Cod_fw = tabella4.Cod_fw) " & _
            "INNER JOIN Dettaglio_rif AS tabella5 ON tabella4.Cod_fw = tabella5.Cod_fw WHERE (Dettaglio_rif.iso_code = '" & Ricerca(0) & "' AND tabella2.iso_code = '" & _
            Ricerca(1) & "' AND tabella3.iso_code = '" & Ricerca(2) & "' AND tabella4.iso_code = '" & Ricerca(3) & "' AND tabella5.iso_code = '" & Ricerca(4) & "')"), myDataBase, adOpenStatic, adLockReadOnly
        Case 6
            myRecordSet.Open ("SELECT DISTINCT dettaglio_rif.cod_fw FROM ((((dettaglio_rif INNER JOIN dettaglio_rif AS tabella2 ON Dettaglio_rif.cod_fw = tabella2.cod_fw) " & _
            "INNER JOIN Dettaglio_rif AS tabella3 ON tabella2.Cod_fw = tabella3.Cod_fw) INNER JOIN Dettaglio_rif AS tabella4 ON tabella3.Cod_fw = tabella4.Cod_fw) " & _
            "INNER JOIN Dettaglio_rif AS tabella5 ON tabella4.Cod_fw = tabella5.Cod_fw) INNER JOIN Dettaglio_rif AS tabella6 ON tabella5.Cod_fw = tabella6.Cod_fw WHERE " & _
            "(Dettaglio_rif.iso_code = '" & Ricerca(0) & "' AND tabella2.iso_code = '" & Ricerca(1) & "' AND tabella3.iso_code = '" & Ricerca(2) & "' AND tabella4.iso_code = '" & _
            Ricerca(3) & "' AND tabella5.iso_code = '" & Ricerca(4) & "' AND tabella6.iso_code = '" & Ricerca(5) & "')"), myDataBase, adOpenStatic, adLockReadOnly
        Case 7
            myRecordSet.Open ("SELECT DISTINCT dettaglio_rif.cod_fw FROM (((((dettaglio_rif INNER JOIN dettaglio_rif AS tabella2 ON Dettaglio_rif.cod_fw = tabella2.cod_fw) " & _
            "INNER JOIN Dettaglio_rif AS tabella3 ON tabella2.Cod_fw = tabella3.Cod_fw) INNER JOIN Dettaglio_rif AS tabella4 ON tabella3.Cod_fw = tabella4.Cod_fw) " & _
            "INNER JOIN Dettaglio_rif AS tabella5 ON tabella4.Cod_fw = tabella5.Cod_fw) INNER JOIN Dettaglio_rif AS tabella6 ON tabella5.Cod_fw = tabella6.Cod_fw) " & _
            "INNER JOIN Dettaglio_rif AS tabella7 ON tabella6.Cod_fw = tabella7.Cod_fw WHERE (Dettaglio_rif.iso_code = '" & Ricerca(0) & "' AND tabella2.iso_code = '" & _
            Ricerca(1) & "' AND tabella3.iso_code = '" & Ricerca(2) & "' AND tabella4.iso_code = '" & Ricerca(3) & "' AND tabella5.iso_code = '" & _
            Ricerca(4) & "' AND tabella6.iso_code = '" & Ricerca(5) & "' AND tabella7.iso_code = '" & Ricerca(6) & "')"), myDataBase, adOpenStatic, adLockReadOnly
        Case 8
            myRecordSet.Open ("SELECT DISTINCT dettaglio_rif.cod_fw FROM ((((((dettaglio_rif INNER JOIN dettaglio_rif AS tabella2 ON Dettaglio_rif.cod_fw = tabella2.cod_fw) " & _
            "INNER JOIN Dettaglio_rif AS tabella3 ON tabella2.Cod_fw = tabella3.Cod_fw) INNER JOIN Dettaglio_rif AS tabella4 ON tabella3.Cod_fw = tabella4.Cod_fw) " & _
            "INNER JOIN Dettaglio_rif AS tabella5 ON tabella4.Cod_fw = tabella5.Cod_fw) INNER JOIN Dettaglio_rif AS tabella6 ON tabella5.Cod_fw = tabella6.Cod_fw) " & _
            "INNER JOIN Dettaglio_rif AS tabella7 ON tabella6.Cod_fw = tabella7.Cod_fw) INNER JOIN Dettaglio_rif AS tabella8 ON tabella7.Cod_fw = tabella8.Cod_fw WHERE " & _
            "(Dettaglio_rif.iso_code = '" & Ricerca(0) & "' AND tabella2.iso_code = '" & Ricerca(1) & "' AND tabella3.iso_code = '" & Ricerca(2) & "' AND tabella4.iso_code = '" & _
            Ricerca(3) & "' AND tabella5.iso_code = '" & Ricerca(4) & "' AND tabella6.iso_code = '" & Ricerca(5) & "' AND tabella7.iso_code = '" & Ricerca(6) & _
            "' AND tabella8.iso_code = '" & Ricerca(7) & "')"), myDataBase, adOpenStatic, adLockReadOnly
        Case 9
            myRecordSet.Open ("SELECT DISTINCT dettaglio_rif.cod_fw FROM ((((((dettaglio_rif INNER JOIN dettaglio_rif AS tabella2 ON Dettaglio_rif.cod_fw = tabella2.cod_fw) " & _
            "INNER JOIN Dettaglio_rif AS tabella3 ON tabella2.Cod_fw = tabella3.Cod_fw) INNER JOIN Dettaglio_rif AS tabella4 ON tabella3.Cod_fw = tabella4.Cod_fw) " & _
            "INNER JOIN Dettaglio_rif AS tabella5 ON tabella4.Cod_fw = tabella5.Cod_fw) INNER JOIN Dettaglio_rif AS tabella6 ON tabella5.Cod_fw = tabella6.Cod_fw) " & _
            "INNER JOIN Dettaglio_rif AS tabella7 ON tabella6.Cod_fw = tabella7.Cod_fw) INNER JOIN Dettaglio_rif AS tabella8 ON tabella7.Cod_fw = tabella8.Cod_fw WHERE " & _
            "(Dettaglio_rif.iso_code = '" & Ricerca(0) & "' AND tabella2.iso_code = '" & Ricerca(1) & "' AND tabella3.iso_code = '" & Ricerca(2) & "' AND tabella4.iso_code = '" & _
            Ricerca(3) & "' AND tabella5.iso_code = '" & Ricerca(4) & "' AND tabella6.iso_code = '" & Ricerca(5) & "' AND tabella7.iso_code = '" & Ricerca(6) & _
            "' AND tabella8.iso_code = '" & Ricerca(7) & "')"), myDataBase, adOpenStatic, adLockReadOnly
        Case 10
            myRecordSet.Open ("SELECT DISTINCT dettaglio_rif.cod_fw FROM (((((((dettaglio_rif INNER JOIN dettaglio_rif AS tabella2 ON Dettaglio_rif.cod_fw = tabella2.cod_fw) " & _
            "INNER JOIN Dettaglio_rif AS tabella3 ON tabella2.Cod_fw = tabella3.Cod_fw) INNER JOIN Dettaglio_rif AS tabella4 ON tabella3.Cod_fw = tabella4.Cod_fw) " & _
            "INNER JOIN Dettaglio_rif AS tabella5 ON tabella4.Cod_fw = tabella5.Cod_fw) INNER JOIN Dettaglio_rif AS tabella6 ON tabella5.Cod_fw = tabella6.Cod_fw) " & _
            "INNER JOIN Dettaglio_rif AS tabella7 ON tabella6.Cod_fw = tabella7.Cod_fw) INNER JOIN Dettaglio_rif AS tabella8 ON tabella7.Cod_fw = tabella8.Cod_fw) " & _
            "INNER JOIN Dettaglio_rif AS tabella9 ON tabella8.Cod_fw = tabella9.Cod_fw WHERE " & _
            "(Dettaglio_rif.iso_code = '" & Ricerca(0) & "' AND tabella2.iso_code = '" & Ricerca(1) & "' AND tabella3.iso_code = '" & Ricerca(2) & "' AND tabella4.iso_code = '" & _
            Ricerca(3) & "' AND tabella5.iso_code = '" & Ricerca(4) & "' AND tabella6.iso_code = '" & Ricerca(5) & "' AND tabella7.iso_code = '" & Ricerca(6) & _
            "' AND tabella8.iso_code = '" & Ricerca(7) & "' AND tabella9.iso_code = '" & Ricerca(8) & "')"), myDataBase, adOpenStatic, adLockReadOnly
    End Select
  
    lstFW.Clear
    
    If chkReader(0).Value = True Then
        If myRecordSet.EOF = False Then
            trovato = True
        End If
        myRecordSet.MoveFirst
        Do Until myRecordSet.EOF = True
'            If FF <> "" Then FF = FF & ","
'            FF = FF & myRecordSet(0).Value
            If myRecordSet.EOF Then Exit Do
            lstFW.AddItem myRecordSet(0).Value
            myRecordSet.MoveNext
        Loop
        myRecordSet.Close
    Else
        Dim filtroModuli, moduli, FF  As String
        Dim codiciModuli() As String
        Dim codiciFF() As String
        If myRecordSet.EOF = False Then
            trovato = True
        End If
        myRecordSet.MoveFirst
        Do Until myRecordSet.EOF = True
            If FF <> "" Then FF = FF & ","
            FF = FF & myRecordSet(0).Value
    '        If myRecordSet.EOF Then Exit Do
    '        lstFW.AddItem myRecordSet(0).Value
            myRecordSet.MoveNext
        Loop
        codiciFF = Split(FF, ",")
        myRecordSet.Close
        
        filtroModuli = "WHERE "
        For i = 1 To 9
            
            If chkReader(i).Value = True Then
                If filtroModuli <> "WHERE " Then
                    filtroModuli = filtroModuli + " OR "
                Else
                    
                End If
                filtroModuli = filtroModuli & "descrizione LIKE '%" & chkReader(i).Caption & "%'"
            End If
        Next
        myRecordSetModuli.Open ("SELECT * FROM moduli " & filtroModuli), myDataBase, adOpenStatic, adLockReadOnly
        If (myRecordSetModuli.RecordCount) > 0 Then
            Do Until myRecordSetModuli.EOF = True
                If moduli <> "" Then moduli = moduli & ","
                moduli = moduli & myRecordSetModuli("id_modulo")
                myRecordSetModuli.MoveNext
            Loop
            codiciModuli = Split(moduli, ",")
        End If
        myRecordSetModuli.Close
        
        
        Dim indiceModuli, indiceCodici As Integer
        Dim stringaRicerca As String
        For indiceModuli = 0 To UBound(codiciModuli)
            If stringaRicerca <> "" Then stringaRicerca = stringaRicerca & " OR "
            stringaRicerca = stringaRicerca & "id_modulo = '" & codiciModuli(indiceModuli) & "'"
        Next
        stringaRicerca = "SELECT DISTINCT cod_fw FROM fw_mod WHERE (" & stringaRicerca
        
        For indiceCodici = 0 To UBound(codiciFF)
        
            myRecordSetRisultato.Open (stringaRicerca & ") AND cod_fw = '" & codiciFF(indiceCodici) & "'"), myDataBase, adOpenStatic, adLockReadOnly
            If myRecordSetRisultato.EOF = False Then
                myRecordSetRisultato.MoveFirst
                lstFW.AddItem myRecordSetRisultato("cod_fw").Value
            End If
            myRecordSetRisultato.Close
        Next
        
        myDataBase.Close
    End If
    If trovato = False Then
        MsgBox "Nessun CDF trovato!", vbCritical, "ATTENZIONE!!!"
    End If
End Sub

Private Sub btnChiudi_Click()
    frmCDF.Hide
    Form1.Enabled = True
End Sub

Private Sub chkReader_Click(Index As Integer)
'    Dim chkValue As Integer
'    If Index = 0 And chkReader(0).Value = 1 Then
'        chkValue = 1
'        Dim i As Integer
'        For i = 1 To 9
'            chkReader(i).Value = chkValue
'        Next
'    End If
'    If Index <> 0 Then
'        If chkReader(Index).Value = 0 Then chkReader(0).Value = 0
'    End If
'    Dim i As Integer
'    i = 1
'    For i = 1 To 9
'        If i <> Index Then chkReader(i).Value = 0
'    Next
End Sub

Private Sub chkValuta_Click(Index As Integer)
    
    If Index > 0 And cboValuta(Index - 1).Text <> "" Then
        cboValuta(Index).Enabled = IIf(chkValuta(Index).Value = 1, True, False)
        If chkValuta(Index).Value = 1 Then
            cboValuta(Index).SetFocus
        End If
    Else
        chkValuta(Index).Value = 0
    End If
    If chkValuta(Index).Value = 0 Then
        Dim i As Integer
        For i = Index To 9
            chkValuta(i).Value = 0
            cboValuta(i).Text = ""
            cboValuta(i).Enabled = False
        Next
    End If
    lstFW.Clear
    lstValute.Clear
End Sub


Private Sub Form_Load()
    databaseFileName = Read_Set("Server", "databasePath")
    databaseFileName = Left(databaseFileName, Len(databaseFileName) - 1)
        
    Set myDataBase = New ADODB.Connection
    Set myRecordSet = New ADODB.Recordset
    myDataBase.Open "Provider =Microsoft.Jet.OLEDB.4.0;Data Source=" & databaseFileName & ";"
    myRecordSet.Open ("SELECT * FROM dettaglio_valuta"), myDataBase, adOpenStatic, adLockReadOnly
    Dim i, z As Integer
    For i = 0 To 9
        cboValuta(i).Clear
    Next
    
    For i = 0 To myRecordSet.RecordCount - 1
        For z = 0 To 9
            cboValuta(z).AddItem myRecordSet("iso_code")
        Next z
        myRecordSet.MoveNext
    Next i
'    myRecordSet.Close
'
'    'aggiungi tipo lettori
'    Dim numChk As Integer
'    numChk = 1
'    myRecordSet.Open ("SELECT * FROM Moduli"), myDataBase, adOpenStatic, adLockReadOnly 'WHERE descrizione LIKE '*RS??' OR descrizione LIKE '*RS???' OR descrizione LIKE 'RS*'
'    For i = 0 To myRecordSet.RecordCount - 1
'        If InStr(1, myRecordSet("descrizione"), "RS") > 0 Then
'            frameReader.Caption = numChk
'            Dim chk As Control
'            Set chk = frmCDF.Controls.Add("VB.checkbox", "chkTemp" & numChk, frameReader)
'            chk.Left = 240
'            chk.Top = 360 + 240 * numChk
'            chk.Caption = myRecordSet("id_modulo")
'            chk.Visible = True
'            chk.Height = 255
'            numChk = numChk + 1
'        End If
'        myRecordSet.MoveNext
'    Next
    
    myRecordSet.Close
    myDataBase.Close
End Sub

Private Sub lstFW_Click()
    databaseFileName = Read_Set("Server", "databasePath")
    databaseFileName = Left(databaseFileName, Len(databaseFileName) - 1)
    lstValute.Clear
    Set myDataBase = New ADODB.Connection
    Set myRecordSet = New ADODB.Recordset
    myDataBase.Open "Provider =Microsoft.Jet.OLEDB.4.0;Data Source=" & databaseFileName & ";"
    myRecordSet.Open ("SELECT * FROM dettaglio_rif WHERE cod_fw ='" & lstFW.Text & "'"), myDataBase, adOpenStatic, adLockReadOnly
    
    Do Until myRecordSet.EOF = True
        If myRecordSet.EOF Then Exit Do
        lstValute.AddItem Left(myRecordSet("iso_code").Value & Space(8), 8) & Space(1) & Left(myRecordSet("versione_iso").Value & Space(6), 6) & Space(1) & myRecordSet("fitness").Value
        myRecordSet.MoveNext
    Loop
    
    myRecordSet.Close
    myDataBase.Close
End Sub
