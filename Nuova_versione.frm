VERSION 5.00
Begin VB.Form Nuova_versione 
   Caption         =   "Tips"
   ClientHeight    =   5925
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9585
   LinkTopic       =   "Form2"
   ScaleHeight     =   5925
   ScaleWidth      =   9585
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton btnOK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Left            =   7770
      TabIndex        =   2
      Top             =   5055
      Width           =   1380
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Hide this message in the future"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   345
      TabIndex        =   1
      Top             =   5235
      Width           =   3255
   End
   Begin VB.TextBox txtMessaggio 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3780
      Left            =   330
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   0
      Text            =   "Nuova_versione.frx":0000
      Top             =   825
      Width           =   8805
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "The software was updated"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   60
      TabIndex        =   3
      Top             =   255
      Width           =   9090
   End
End
Attribute VB_Name = "Nuova_versione"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub btnOK_Click()
    If Check1.Value = 1 Then
        aa = Write_Set("c:\database_fw\database_fw.ini", "Tips", "Version " & App.Major & "." & App.Minor & "." & App.Revision, "1")
    End If
    
    Nuova_versione.Visible = False
    Unload Nuova_versione
End Sub

Private Sub Form_Load()

'    Text1.Text = "Il software dispone di una nuova funzionalitą:" & Chr(13) & Chr(10) & "E' stato introdotto nel database una nuova categoria di 'Firmware': FW_personalizz. Permette di ricercare i soli file di personalizzazione (filmati, immagini ecc..) codificati FFXXXX."
    'Text1.Text = "Variata cartella memorizzazione scelta"
    'Text1.Text = "Aggiunto campo per la configurazione delle piastre elettroniche di ricambio"
    'txtMessaggio.Text = "- Aggiunta la visualizzazione della unit configuration tramite il doppio click del numero 'unit config' nella ricerca prodotto" & Chr(13) & Chr(10) & "- Aggiunta la ricerca del codice CDF FFxxx con l'applicazione di filtri di ricerca per valute"
    'txtMessaggio.Text = "- Aggiunta la visualizzazione della configurazione della customizazione (EndUserCustomization)" & Chr(13) & Chr(10) & "- Aggiunta la visualizzazione della configurazione del CC3R"
    txtMessaggio.Text = "- Aggiunta la ricerca del codice CDF FFxxx con l'applicazione di filtri di ricerca per tipo lettore"
End Sub

