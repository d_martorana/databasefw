VERSION 5.00
Begin VB.Form UnitConfig 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "UNIT CONFIG"
   ClientHeight    =   7125
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   15705
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7125
   ScaleWidth      =   15705
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame4 
      Caption         =   "CM OPTION TWO CONFIG"
      Enabled         =   0   'False
      Height          =   6135
      Left            =   9240
      TabIndex        =   36
      Top             =   840
      Width           =   5310
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   31
         Left            =   2760
         TabIndex        =   68
         Top             =   5760
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   30
         Left            =   2760
         TabIndex        =   67
         Top             =   5400
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   29
         Left            =   2760
         TabIndex        =   66
         Top             =   5040
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   28
         Left            =   2760
         TabIndex        =   65
         Top             =   4680
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   27
         Left            =   2760
         TabIndex        =   64
         Top             =   4320
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   26
         Left            =   2760
         TabIndex        =   63
         Top             =   3960
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   25
         Left            =   2760
         TabIndex        =   62
         Top             =   3600
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   24
         Left            =   2760
         TabIndex        =   61
         Top             =   3240
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   23
         Left            =   2760
         TabIndex        =   60
         Top             =   2880
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Disable CM18EVO led pulse"
         Height          =   255
         Index           =   22
         Left            =   2760
         TabIndex        =   59
         Top             =   2520
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Handling new LED"
         Height          =   255
         Index           =   21
         Left            =   2760
         TabIndex        =   58
         Top             =   2160
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Virtual Desktop Environment"
         Height          =   255
         Index           =   20
         Left            =   2760
         TabIndex        =   57
         Top             =   1800
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   19
         Left            =   2760
         TabIndex        =   56
         Top             =   1440
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   18
         Left            =   2760
         TabIndex        =   55
         Top             =   1080
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Remap feed jam missfeeding"
         Height          =   255
         Index           =   17
         Left            =   2760
         TabIndex        =   54
         Top             =   720
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Alarm 2 output handling"
         Height          =   255
         Index           =   16
         Left            =   2760
         TabIndex        =   53
         Top             =   360
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Disable display message "
         Height          =   255
         Index           =   15
         Left            =   120
         TabIndex        =   52
         Top             =   5760
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   14
         Left            =   120
         TabIndex        =   51
         Top             =   5400
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Shutter handling"
         Height          =   255
         Index           =   13
         Left            =   120
         TabIndex        =   50
         Top             =   5040
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Block recovery pow off cash"
         Height          =   255
         Index           =   12
         Left            =   120
         TabIndex        =   49
         Top             =   4680
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   11
         Left            =   120
         TabIndex        =   48
         Top             =   4320
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Different center AU bn"
         Height          =   255
         Index           =   10
         Left            =   120
         TabIndex        =   47
         Top             =   3960
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Feed bn with input bin free"
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   46
         Top             =   3600
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   45
         Top             =   3240
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Separate Cat2 and Cat3"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   44
         Top             =   2880
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "EVO LED handling"
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   43
         Top             =   2520
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   42
         Top             =   2160
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Handling cat 2 box"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   41
         Top             =   1800
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Special clean using display"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   40
         Top             =   1440
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Banknotes orientation in safe"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   39
         Top             =   1080
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "BVU synchronous handling"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   38
         Top             =   720
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionTwo 
         Caption         =   "Enable 40mm safe"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   37
         Top             =   360
         Width           =   2415
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "CM OPTION ONE CONFIG"
      Enabled         =   0   'False
      Height          =   6135
      Left            =   6480
      TabIndex        =   34
      Top             =   840
      Width           =   2655
      Begin VB.CheckBox chkCMOptionOne 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   15
         Left            =   120
         TabIndex        =   83
         Top             =   5760
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionOne 
         Caption         =   "Enable Open extenal button"
         Height          =   255
         Index           =   14
         Left            =   120
         TabIndex        =   82
         Top             =   5400
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionOne 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   13
         Left            =   120
         TabIndex        =   81
         Top             =   5040
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionOne 
         Caption         =   "Enable TCDA"
         Height          =   255
         Index           =   12
         Left            =   120
         TabIndex        =   80
         Top             =   4680
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionOne 
         Caption         =   "Timeout connection LAN"
         Height          =   255
         Index           =   11
         Left            =   120
         TabIndex        =   79
         Top             =   4320
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionOne 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   10
         Left            =   120
         TabIndex        =   78
         Top             =   3960
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionOne 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   77
         Top             =   3600
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionOne 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   76
         Top             =   3240
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionOne 
         Caption         =   "Don't remove cass jam"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   75
         Top             =   2880
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionOne 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   74
         Top             =   2520
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionOne 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   73
         Top             =   2160
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionOne 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   72
         Top             =   1800
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionOne 
         Caption         =   "Impact 0mm on CR37"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   71
         Top             =   1440
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionOne 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   70
         Top             =   1080
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionOne 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   69
         Top             =   720
         Width           =   2415
      End
      Begin VB.CheckBox chkCMOptionOne 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   35
         Top             =   360
         Width           =   2415
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "CM OPTION CONFIG"
      Enabled         =   0   'False
      Height          =   6135
      Left            =   3720
      TabIndex        =   17
      Top             =   840
      Width           =   2655
      Begin VB.CheckBox chkCMOptionConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   15
         Left            =   120
         TabIndex        =   33
         Top             =   5760
         Width           =   2295
      End
      Begin VB.CheckBox chkCMOptionConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   14
         Left            =   120
         TabIndex        =   32
         Top             =   5400
         Width           =   2295
      End
      Begin VB.CheckBox chkCMOptionConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   13
         Left            =   120
         TabIndex        =   31
         Top             =   5040
         Width           =   2295
      End
      Begin VB.CheckBox chkCMOptionConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   12
         Left            =   120
         TabIndex        =   30
         Top             =   4680
         Width           =   2295
      End
      Begin VB.CheckBox chkCMOptionConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   11
         Left            =   120
         TabIndex        =   29
         Top             =   4320
         Width           =   2295
      End
      Begin VB.CheckBox chkCMOptionConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   10
         Left            =   120
         TabIndex        =   28
         Top             =   3960
         Width           =   2295
      End
      Begin VB.CheckBox chkCMOptionConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   27
         Top             =   3600
         Width           =   2295
      End
      Begin VB.CheckBox chkCMOptionConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   26
         Top             =   3240
         Width           =   2295
      End
      Begin VB.CheckBox chkCMOptionConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   25
         Top             =   2880
         Width           =   2295
      End
      Begin VB.CheckBox chkCMOptionConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   24
         Top             =   2520
         Width           =   2295
      End
      Begin VB.CheckBox chkCMOptionConfig 
         Caption         =   "Execute CLOSE in JAM"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   23
         Top             =   2160
         Width           =   2295
      End
      Begin VB.CheckBox chkCMOptionConfig 
         Caption         =   "SIMPLIFIED protocol"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   22
         Top             =   1800
         Width           =   2295
      End
      Begin VB.CheckBox chkCMOptionConfig 
         Caption         =   "Special alarm mode"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   21
         Top             =   1440
         Width           =   2295
      End
      Begin VB.CheckBox chkCMOptionConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   20
         Top             =   1080
         Width           =   2295
      End
      Begin VB.CheckBox chkCMOptionConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   19
         Top             =   720
         Width           =   2295
      End
      Begin VB.CheckBox chkCMOptionConfig 
         Caption         =   "Spec Clean without control"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   18
         Top             =   360
         Width           =   2295
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "CM CONFIG"
      Enabled         =   0   'False
      Height          =   6135
      Left            =   960
      TabIndex        =   0
      Top             =   840
      Width           =   2655
      Begin VB.CheckBox chkCMConfig 
         Caption         =   "Enable JOURNAL LOG"
         Height          =   255
         Index           =   15
         Left            =   120
         TabIndex        =   16
         Top             =   5760
         Width           =   2295
      End
      Begin VB.CheckBox chkCMConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   14
         Left            =   120
         TabIndex        =   15
         Top             =   5400
         Width           =   2295
      End
      Begin VB.CheckBox chkCMConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   13
         Left            =   120
         TabIndex        =   14
         Top             =   5040
         Width           =   2295
      End
      Begin VB.CheckBox chkCMConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   12
         Left            =   120
         TabIndex        =   13
         Top             =   4680
         Width           =   2295
      End
      Begin VB.CheckBox chkCMConfig 
         Caption         =   "Use UNFIT in safe"
         Height          =   255
         Index           =   11
         Left            =   120
         TabIndex        =   12
         Top             =   4320
         Width           =   2295
      End
      Begin VB.CheckBox chkCMConfig 
         Caption         =   "Date/Time in AM/PM format"
         Height          =   255
         Index           =   10
         Left            =   120
         TabIndex        =   11
         Top             =   3960
         Width           =   2415
      End
      Begin VB.CheckBox chkCMConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   10
         Top             =   3600
         Width           =   2295
      End
      Begin VB.CheckBox chkCMConfig 
         Caption         =   "Use delay class dispensing"
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   9
         Top             =   3240
         Width           =   2295
      End
      Begin VB.CheckBox chkCMConfig 
         Caption         =   "Alarm 1 handling"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   8
         Top             =   2880
         Width           =   2295
      End
      Begin VB.CheckBox chkCMConfig 
         Caption         =   "Balanced cassette handling"
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   7
         Top             =   2520
         Width           =   2295
      End
      Begin VB.CheckBox chkCMConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   6
         Top             =   2160
         Width           =   2295
      End
      Begin VB.CheckBox chkCMConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   5
         Top             =   1800
         Width           =   2295
      End
      Begin VB.CheckBox chkCMConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   4
         Top             =   1440
         Width           =   2295
      End
      Begin VB.CheckBox chkCMConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   3
         Top             =   1080
         Width           =   2295
      End
      Begin VB.CheckBox chkCMConfig 
         Caption         =   "Reserved"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   2
         Top             =   720
         Width           =   2295
      End
      Begin VB.CheckBox chkCMConfig 
         Caption         =   "View date/time on display"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Width           =   2295
      End
   End
   Begin VB.Label lblDescription 
      Caption         =   "999"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6480
      TabIndex        =   121
      Top             =   240
      Width           =   9255
   End
   Begin VB.Label Label37 
      Caption         =   "DESCRIPTION:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4320
      TabIndex        =   120
      Top             =   240
      Width           =   2055
   End
   Begin VB.Label lblIdConfig 
      Caption         =   "999"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2640
      TabIndex        =   119
      Top             =   240
      Width           =   975
   End
   Begin VB.Label Label35 
      Caption         =   "ID UNIT CONFIG:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   118
      Top             =   240
      Width           =   2295
   End
   Begin VB.Label Label34 
      Caption         =   "Address"
      Height          =   255
      Left            =   14640
      TabIndex        =   117
      Top             =   840
      Width           =   975
   End
   Begin VB.Label Label33 
      Alignment       =   1  'Right Justify
      Caption         =   "Address"
      Height          =   255
      Left            =   120
      TabIndex        =   116
      Top             =   840
      Width           =   615
   End
   Begin VB.Label Label32 
      Caption         =   "0x00080000"
      Height          =   255
      Left            =   14640
      TabIndex        =   115
      Top             =   2280
      Width           =   975
   End
   Begin VB.Label Label31 
      Caption         =   "0x00040000"
      Height          =   255
      Left            =   14640
      TabIndex        =   114
      Top             =   1920
      Width           =   975
   End
   Begin VB.Label Label30 
      Caption         =   "0x00020000"
      Height          =   255
      Left            =   14640
      TabIndex        =   113
      Top             =   1560
      Width           =   975
   End
   Begin VB.Label Label29 
      Caption         =   "0x00010000"
      Height          =   255
      Left            =   14640
      TabIndex        =   112
      Top             =   1200
      Width           =   975
   End
   Begin VB.Label Label28 
      Caption         =   "0x80000000"
      Height          =   255
      Left            =   14640
      TabIndex        =   111
      Top             =   6600
      Width           =   975
   End
   Begin VB.Label Label27 
      Caption         =   "0x40000000"
      Height          =   255
      Left            =   14640
      TabIndex        =   110
      Top             =   6240
      Width           =   975
   End
   Begin VB.Label Label26 
      Caption         =   "0x20000000"
      Height          =   255
      Left            =   14640
      TabIndex        =   109
      Top             =   5880
      Width           =   975
   End
   Begin VB.Label Label25 
      Caption         =   "0x10000000"
      Height          =   255
      Left            =   14640
      TabIndex        =   108
      Top             =   5520
      Width           =   975
   End
   Begin VB.Label Label24 
      Caption         =   "0x08000000"
      Height          =   255
      Left            =   14640
      TabIndex        =   107
      Top             =   5160
      Width           =   975
   End
   Begin VB.Label Label23 
      Caption         =   "0x04000000"
      Height          =   255
      Left            =   14640
      TabIndex        =   106
      Top             =   4800
      Width           =   975
   End
   Begin VB.Label Label22 
      Caption         =   "0x02000000"
      Height          =   255
      Left            =   14640
      TabIndex        =   105
      Top             =   4440
      Width           =   975
   End
   Begin VB.Label Label21 
      Caption         =   "0x01000000"
      Height          =   255
      Left            =   14640
      TabIndex        =   104
      Top             =   4080
      Width           =   975
   End
   Begin VB.Label Label20 
      Caption         =   "0x00800000"
      Height          =   255
      Left            =   14640
      TabIndex        =   103
      Top             =   3720
      Width           =   975
   End
   Begin VB.Label Label19 
      Caption         =   "0x00400000"
      Height          =   255
      Left            =   14640
      TabIndex        =   102
      Top             =   3360
      Width           =   975
   End
   Begin VB.Label Label18 
      Caption         =   "0x00200000"
      Height          =   255
      Left            =   14640
      TabIndex        =   101
      Top             =   3000
      Width           =   975
   End
   Begin VB.Label Label17 
      Caption         =   "0x00100000"
      Height          =   255
      Left            =   14640
      TabIndex        =   100
      Top             =   2640
      Width           =   975
   End
   Begin VB.Label Label16 
      Alignment       =   1  'Right Justify
      Caption         =   "0x8000"
      Height          =   255
      Left            =   120
      TabIndex        =   99
      Top             =   6600
      Width           =   615
   End
   Begin VB.Label Label15 
      Alignment       =   1  'Right Justify
      Caption         =   "0x4000"
      Height          =   255
      Left            =   120
      TabIndex        =   98
      Top             =   6240
      Width           =   615
   End
   Begin VB.Label Label14 
      Alignment       =   1  'Right Justify
      Caption         =   "0x2000"
      Height          =   255
      Left            =   120
      TabIndex        =   97
      Top             =   5880
      Width           =   615
   End
   Begin VB.Label Label13 
      Alignment       =   1  'Right Justify
      Caption         =   "0x1000"
      Height          =   255
      Left            =   120
      TabIndex        =   96
      Top             =   5520
      Width           =   615
   End
   Begin VB.Label Label12 
      Alignment       =   1  'Right Justify
      Caption         =   "0x0800"
      Height          =   255
      Left            =   120
      TabIndex        =   95
      Top             =   5160
      Width           =   615
   End
   Begin VB.Label Label11 
      Alignment       =   1  'Right Justify
      Caption         =   "0x0400"
      Height          =   255
      Left            =   120
      TabIndex        =   94
      Top             =   4800
      Width           =   615
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "0x0200"
      Height          =   255
      Left            =   120
      TabIndex        =   93
      Top             =   4440
      Width           =   615
   End
   Begin VB.Label Label9 
      Alignment       =   1  'Right Justify
      Caption         =   "0x0100"
      Height          =   255
      Left            =   120
      TabIndex        =   92
      Top             =   4080
      Width           =   615
   End
   Begin VB.Label Label8 
      Alignment       =   1  'Right Justify
      Caption         =   "0x0080"
      Height          =   255
      Left            =   120
      TabIndex        =   91
      Top             =   3720
      Width           =   615
   End
   Begin VB.Label Label7 
      Alignment       =   1  'Right Justify
      Caption         =   "0x0040"
      Height          =   255
      Left            =   120
      TabIndex        =   90
      Top             =   3360
      Width           =   615
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "0x0020"
      Height          =   255
      Left            =   120
      TabIndex        =   89
      Top             =   3000
      Width           =   615
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      Caption         =   "0x0010"
      Height          =   255
      Left            =   120
      TabIndex        =   88
      Top             =   2640
      Width           =   615
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      Caption         =   "0x0008"
      Height          =   255
      Left            =   120
      TabIndex        =   87
      Top             =   2280
      Width           =   615
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "0x0004"
      Height          =   255
      Left            =   120
      TabIndex        =   86
      Top             =   1920
      Width           =   615
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "0x0002"
      Height          =   255
      Left            =   120
      TabIndex        =   85
      Top             =   1560
      Width           =   615
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "0x0001"
      Height          =   255
      Left            =   120
      TabIndex        =   84
      Top             =   1200
      Width           =   615
   End
End
Attribute VB_Name = "UnitConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public idConfig As String

Private Sub Form_Load()
    Ricerca.Enabled = False
    Dim myDataBase As ADODB.Connection
    Dim myRecordSet As ADODB.Recordset
    Dim databaseFileName As String
    'lblIdConfig.Caption = idConfig
    On Error GoTo errore
    'databaseFileName = Read_Set("Server", "databasePath")
    'databaseFileName = Left(databaseFileName, Len(databaseFileName) - 1)
    databaseFileName = "c:\database_fw\man1220_dati.mdb"
    
    Set myDataBase = New ADODB.Connection
    Set myRecordSet = New ADODB.Recordset
    myDataBase.Open "Provider =Microsoft.Jet.OLEDB.4.0;Data Source=" & databaseFileName & ";"
    myRecordSet.Open ("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " & idConfig), myDataBase, adOpenStatic, adLockReadOnly
    lblIdConfig.Caption = myRecordSet("id_nome_unit_config")
    lblDescription.Caption = myRecordSet("nome_unit_config")
    chkCMConfig(0).Value = IIf(myRecordSet("View date & time on display") = True, 1, 0) '
    chkCMConfig(6).Value = IIf(myRecordSet("balance cassette handling") = True, 1, 0)
    chkCMConfig(7).Value = IIf(myRecordSet("Alarm1 handling") = True, 1, 0)
    chkCMConfig(8).Value = IIf(myRecordSet("use delay class dispensing") = True, 1, 0)
    chkCMConfig(10).Value = IIf(myRecordSet("Data & time on display in format AM and PM") = True, 1, 0)
    chkCMConfig(11).Value = IIf(myRecordSet("Use Unfit in safe") = True, 1, 0)
    chkCMConfig(15).Value = IIf(myRecordSet("Enable journal log") = True, 1, 0)
    
    chkCMOptionConfig(0).Value = IIf(myRecordSet("Special clean without control") = True, 1, 0)
    chkCMOptionConfig(3).Value = IIf(myRecordSet("Special alarm mode") = True, 1, 0)
    chkCMOptionConfig(4).Value = IIf(myRecordSet("identify STD/SIMPLIFIED protocol") = True, 1, 0)
    chkCMOptionConfig(5).Value = IIf(myRecordSet("Execute close command also in jam") = True, 1, 0)
    
    chkCMOptionOne(3).Value = IIf(myRecordSet("Impac  0mm on cassette CR37 in set_cfg") = True, 1, 0)
    chkCMOptionOne(7).Value = IIf(myRecordSet("don't remove cass jam when special clean without control ena") = True, 1, 0)
    chkCMOptionOne(10).Value = IIf(myRecordSet("ssl protocol (LAN)") = True, 1, 0)
    chkCMOptionOne(11).Value = IIf(myRecordSet("timeout connect/disconnect LAN") = True, 1, 0)
    chkCMOptionOne(12).Value = IIf(myRecordSet("enable time control dispence amount (TCDA)") = True, 1, 0)
    chkCMOptionOne(14).Value = IIf(myRecordSet("Enable booking open by external button") = True, 1, 0)
    
    Dim i As Integer
    For i = 0 To 31
        chkCMOptionTwo(i).Value = IIf(myRecordSet(i + 19) = True, 1, 0)
    Next
    
    myRecordSet.Close
errore:
    If err.Number > 0 Then MsgBox "lettura unit configuration non andata a buon fine"
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Ricerca.Enabled = True
End Sub
