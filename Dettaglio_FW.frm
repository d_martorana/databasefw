VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form Dettaglio_FW 
   Caption         =   "Ricerca firmware - Database FW"
   ClientHeight    =   10410
   ClientLeft      =   465
   ClientTop       =   1050
   ClientWidth     =   14130
   LinkTopic       =   "Form2"
   ScaleHeight     =   10410
   ScaleWidth      =   14130
   WindowState     =   2  'Maximized
   Begin MSComDlg.CommonDialog salva1 
      Left            =   1755
      Top             =   8790
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0FFC0&
      BorderStyle     =   0  'None
      Caption         =   "Ricerca Firmware"
      Height          =   2220
      Left            =   705
      TabIndex        =   0
      Top             =   390
      Width           =   9240
      Begin VB.ComboBox Combo1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         ItemData        =   "Dettaglio_FW.frx":0000
         Left            =   105
         List            =   "Dettaglio_FW.frx":0002
         TabIndex        =   10
         Top             =   690
         Width           =   3540
      End
      Begin VB.ComboBox Combo9 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   3840
         TabIndex        =   6
         Top             =   1455
         Width           =   5280
      End
      Begin VB.ComboBox Combo8 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         ItemData        =   "Dettaglio_FW.frx":0004
         Left            =   105
         List            =   "Dettaglio_FW.frx":0006
         TabIndex        =   4
         Top             =   1455
         Width           =   3510
      End
      Begin VB.ComboBox Combo7 
         Height          =   315
         Left            =   11955
         TabIndex        =   2
         Top             =   630
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Select Firmware type"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   105
         TabIndex        =   11
         Top             =   345
         Width           =   4395
      End
      Begin VB.Label Label11 
         BackStyle       =   0  'Transparent
         Caption         =   "Search key"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3840
         TabIndex        =   7
         Top             =   1125
         Width           =   2280
      End
      Begin VB.Label Label10 
         BackStyle       =   0  'Transparent
         Caption         =   "Search by"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   90
         TabIndex        =   5
         Top             =   1125
         Width           =   1605
      End
      Begin VB.Label Label8 
         Caption         =   "Modulo associato"
         Height          =   225
         Left            =   11940
         TabIndex        =   3
         Top             =   390
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.Label Label4 
         Caption         =   "Label4"
         Height          =   30
         Left            =   4035
         TabIndex        =   1
         Top             =   1695
         Width           =   315
      End
   End
   Begin MSDataGridLib.DataGrid DataGrid2 
      Height          =   1290
      Left            =   12390
      TabIndex        =   8
      Top             =   3150
      Visible         =   0   'False
      Width           =   11880
      _ExtentX        =   20955
      _ExtentY        =   2275
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1040
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         RecordSelectors =   0   'False
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid dataGrid1 
      CausesValidation=   0   'False
      Height          =   5295
      Left            =   675
      TabIndex        =   13
      Top             =   3570
      Width           =   12360
      _ExtentX        =   21802
      _ExtentY        =   9340
      _Version        =   393216
      Cols            =   1
      FixedCols       =   0
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLines       =   2
      GridLinesUnpopulated=   2
      MergeCells      =   4
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   1
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "Double click for details"
      ForeColor       =   &H00C00000&
      Height          =   450
      Left            =   720
      TabIndex        =   12
      Top             =   2700
      Width           =   1485
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "OFFICIAL FILES ARCHIVE"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      Left            =   10410
      TabIndex        =   9
      Top             =   1620
      Width           =   4725
   End
   Begin VB.Image Image3 
      Height          =   630
      Left            =   12375
      Picture         =   "Dettaglio_FW.frx":0008
      Top             =   225
      Width           =   2325
   End
   Begin VB.Menu File 
      Caption         =   "File"
      Begin VB.Menu sta 
         Caption         =   "Print"
      End
      Begin VB.Menu ex 
         Caption         =   "Save as Excel format"
      End
   End
   Begin VB.Menu Ricerca 
      Caption         =   "Search"
      Begin VB.Menu SQL 
         Caption         =   "Advanced SQL search"
      End
   End
   Begin VB.Menu help 
      Caption         =   "Help"
      Begin VB.Menu Versione 
         Caption         =   "Version"
      End
      Begin VB.Menu ultima 
         Caption         =   "Last modify"
      End
   End
End
Attribute VB_Name = "Dettaglio_FW"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim db1 As ADODB.Connection
Dim rs1 As ADODB.Recordset
Dim rs2 As ADODB.Recordset
Dim rs3 As ADODB.Recordset
Dim rs4 As ADODB.Recordset

Private Sub Combo1_Click()
On Error Resume Next
Select Case Combo1.Text

    Case "Firmware", "Messaggi display", "Suite", "Personalizzazione"
        DataGrid2.Visible = False
        Frame1.Caption = "Ricerca Firmware"
        Frame1.Visible = True
        Combo8.Clear
        Combo8.AddItem "Codice firmware"
        Combo8.AddItem "Nome interno"
        Combo8.AddItem "Release"
        Combo8.AddItem "Nome interno associato"
        Combo8.AddItem "Modulo"
        
    Case "CDF"
        DataGrid2.Visible = True
        Frame1.Caption = "Ricerca CDF"
        Frame1.Visible = True
        Combo8.Clear
        Combo8.AddItem "Codice CDF"
        Combo8.AddItem "Codice vendita"
        Combo8.AddItem "Nome interno"
        Combo8.AddItem "Tipo CDF"
        Combo8.AddItem "Codice ISO"
        Combo8.AddItem "Codice ISO sing./mult."
        Combo8.AddItem "Paese"
        Combo8.AddItem "Valuta"
        Combo8.AddItem "Modulo"

    Case "Messaggi display"
        DataGrid2.Visible = False
    Case "Suite"
        DataGrid2.Visible = False
End Select
End Sub


Private Sub Combo8_Change()

    aa = conv(Combo8.ListIndex)

End Sub

Private Sub Combo8_Click()

On Error Resume Next
aa = conv(Combo8.ListIndex)

rs1.Close
Combo9.Clear

Select Case Combo1.ListIndex
    Case 0
    DataGrid2.Visible = False
        Select Case Combo8.ListIndex
        Case 0, 1, 2
        
            rs1.Open ("select DISTINCT " & aa & " from firmware where firmware.tipo_fw = 1"), db1, adOpenStatic, adLockReadOnly
        
        Case 3
            
            rs1.Open ("select DISTINCT " & aa & " from tbldett_fw, firmware where tbldett_fw.cod_fw = firmware.cod_fw and firmware.tipo_fw =1"), db1, adOpenStatic, adLockReadOnly
        
        Case 4
        
            rs1.Open ("select DISTINCT " & aa & " from fw_mod, firmware where fw_mod.cod_fw =firmware.cod_fw and firmware.tipo_fw =1"), db1, adOpenStatic, adLockReadOnly
        
        
        End Select
    Case 1
    DataGrid2.Visible = True
        Select Case Combo8.ListIndex
        Case 0, 1, 2, 3

            rs1.Open ("select DISTINCT " & aa & " from firmware where firmware.tipo_fw = 2"), db1, adOpenStatic, adLockReadOnly
        
        Case 4
            
            rs1.Open ("select DISTINCT " & aa & " from firmware, dettaglio_rif, dettaglio_valuta where dettaglio_rif.iso_code = dettaglio_valuta.iso_code and firmware.cod_fw = dettaglio_rif.cod_fw and firmware.tipo_fw = 2"), db1, adOpenStatic, adLockReadOnly

        Case 6, 7
    
            rs1.Open ("select DISTINCT " & aa & " from firmware, dettaglio_rif, dettaglio_valuta where dettaglio_rif.iso_code = dettaglio_valuta.iso_code and firmware.cod_fw = dettaglio_rif.cod_fw and firmware.tipo_fw = 2"), db1, adOpenStatic, adLockReadOnly
        Case 5
            NOINSERT = True
            rs1.Open ("select firmware.cod_fw as Cod_FW  from firmware where firmware.tipo_fw = 2"), db1, adOpenStatic, adLockReadOnly
            For a = 0 To rs1.RecordCount - 1
                rs2.Close
                HH = "select dettaglio_rif.iso_code from dettaglio_rif where dettaglio_rif.cod_fw = '" & rs1.Fields(0).Value & "'"
                rs2.Open ("select dettaglio_rif.iso_code from dettaglio_rif where dettaglio_rif.cod_fw = '" & rs1.Fields(0).Value & "'"), db1, adOpenStatic, adLockReadOnly
                kk = ""
                    For B = 0 To rs2.RecordCount - 1
                        kk = kk + rs2.Fields(0).Value + " "
                        rs2.MoveNext
                    Next
                    
                    If Len(kk) > 1 Then
                        For g = 0 To Combo9.ListCount - 1
                            If kk = Combo9.List(g) Then GoTo dd
                            
                        Next
                        Combo9.AddItem kk
dd:
                    End If
                rs1.MoveNext
            Next
            Case 8
                    rs1.Open ("select DISTINCT " & aa & " from fw_mod, firmware where fw_mod.cod_fw =firmware.cod_fw and firmware.tipo_fw =2"), db1, adOpenStatic, adLockReadOnly

        End Select
    Case 2
        DataGrid2.Visible = False
        Select Case Combo8.ListIndex
        Case 0, 1, 2
        
            rs1.Open ("select DISTINCT " & aa & " from firmware where firmware.tipo_fw = 3"), db1, adOpenStatic, adLockReadOnly
        
        Case 3
            
            rs1.Open ("select DISTINCT " & aa & " from tbldett_fw, firmware where tbldett_fw.cod_fw = firmware.cod_fw and firmware.tipo_fw =3"), db1, adOpenStatic, adLockReadOnly
        
        Case 4
        
            rs1.Open ("select DISTINCT " & aa & " from fw_mod, firmware where fw_mod.cod_fw =firmware.cod_fw and firmware.tipo_fw =3"), db1, adOpenStatic, adLockReadOnly
        
        
        End Select
    
    Case 3
                DataGrid2.Visible = False
        Select Case Combo8.ListIndex
        Case 0, 1, 2
        
            rs1.Open ("select DISTINCT " & aa & " from firmware where firmware.tipo_fw = 4"), db1, adOpenStatic, adLockReadOnly
        
        Case 3
            
            rs1.Open ("select DISTINCT " & aa & " from tbldett_fw, firmware where tbldett_fw.cod_fw = firmware.cod_fw and firmware.tipo_fw =4"), db1, adOpenStatic, adLockReadOnly
        
        Case 4
        
            rs1.Open ("select DISTINCT " & aa & " from fw_mod, firmware where fw_mod.cod_fw = firmware.cod_fw and firmware.tipo_fw =4"), db1, adOpenStatic, adLockReadOnly
        
        
        End Select
        
    Case 4
        DataGrid2.Visible = False
        Select Case Combo8.ListIndex
        Case 0, 1, 2
        
            rs1.Open ("select DISTINCT " & aa & " from firmware where firmware.tipo_fw = 5"), db1, adOpenStatic, adLockReadOnly
        
        Case 3
            
            rs1.Open ("select DISTINCT " & aa & " from tbldett_fw, firmware where tbldett_fw.cod_fw = firmware.cod_fw and firmware.tipo_fw =5"), db1, adOpenStatic, adLockReadOnly
        
        Case 4
        
            rs1.Open ("select DISTINCT " & aa & " from fw_mod, firmware where fw_mod.cod_fw =firmware.cod_fw and firmware.tipo_fw =5"), db1, adOpenStatic, adLockReadOnly
        
        
        End Select

End Select

If NOINSERT = False Then
    For a = 0 To rs1.RecordCount - 1
        Combo9.AddItem rs1.Fields(0).Value
        rs1.MoveNext
    Next
End If

End Sub

Private Sub Combo9_Click()

On Error Resume Next

7 rs3.Close

Select Case Combo1.ListIndex
    Case 0
        Select Case Combo8.ListIndex
            Case 0, 1, 2
            
                rs3.Open ("select firmware.cod_fw as Cod_FW, firmware.nome_intero as Nome_interno, firmware.versione, firmware.release from firmware where " & conv(Combo8.ListIndex) & " = '" & Combo9.Text & "' order by firmware.cod_fw"), db1, adOpenStatic, adLockReadOnly
        
            Case 3
            
                rs3.Open ("select firmware.cod_fw as Cod_FW, firmware.nome_intero as Nome_interno, firmware.versione, firmware.release from firmware, tbldett_fw where " & conv(Combo8.ListIndex) & " = '" & Combo9.Text & "' and firmware.cod_fw = tbldett_fw.cod_fw order by firmware.cod_fw"), db1, adOpenStatic, adLockReadOnly

            Case 4
            
                rs3.Open ("select firmware.cod_fw as Cod_FW, firmware.nome_intero as Nome_interno, firmware.versione, firmware.release from firmware, fw_mod where " & conv(Combo8.ListIndex) & " = '" & Combo9.Text & "' and firmware.cod_fw = fw_mod.cod_fw and firmware.tipo_fw=1 order by firmware.cod_fw"), db1, adOpenStatic, adLockReadOnly
        
        End Select
    rs3.Requery
    DataGrid1.ColWidth(0) = DataGrid1.Width * 10 / 100
    DataGrid1.ColWidth(1) = DataGrid1.Width * 66 / 100
    DataGrid1.ColWidth(2) = DataGrid1.Width * 10 / 100
    DataGrid1.ColWidth(3) = DataGrid1.Width * 10 / 100


    Case 1
    
        Select Case Combo8.ListIndex
            Case 0, 1, 2, 3, 4, 6, 7

                rs3.Open ("select firmware.cod_fw as Cod_FW, firmware.cod_vendita, firmware.nome_intero as Nome_interno, firmware.tipo_cdf, dettaglio_rif.iso_code, dettaglio_rif.versione_iso, dettaglio_rif.fitness from firmware, dettaglio_rif, dettaglio_valuta where dettaglio_valuta.iso_code = dettaglio_rif.iso_code and firmware.cod_fw = dettaglio_rif.cod_fw and firmware.tipo_fw = 2 and " & conv(Combo8.ListIndex) & " = '" & Combo9.Text & "' order by firmware.cod_fw"), db1, adOpenStatic, adLockReadOnly
                
            Case 5
                
                stringa = ""
                For a = 1 To Len(Combo9.Text) Step 4
                    stringa = stringa & "dettaglio_rif.iso_code = '" & Mid$(Combo9.Text, a, 3) & "' Or "
                Next
                stringa = Left(stringa, Len(stringa) - 4)
                FF = ("select distinct dettaglio_rif.cod_fw from dettaglio_rif where " & stringa)
                rs1.Close
                rs1.Open ("select DISTINCT dettaglio_rif.cod_fw from dettaglio_rif where " & stringa), db1, adOpenStatic, adLockReadOnly
                
                Dim vett() As String
                sel = 0
                rs1.MoveFirst
                For a = 0 To rs1.RecordCount - 1
                    rs2.Close
                    FF = ("select from dettaglio_rif.cod_fw, from dettaglio_rif.iso_code from dettaglio_rif where dettaglio_rif.cod_fw = '" & rs1.Fields(0).Value & "' order by firmware.cod_fw")
                    rs2.Open ("select dettaglio_rif.cod_fw, dettaglio_rif.iso_code from dettaglio_rif where dettaglio_rif.cod_fw = '" & rs1.Fields(0).Value & "' order by firmware.cod_fw"), db1, adOpenStatic, adLockReadOnly
                    If rs2.RecordCount = Len(Combo9.Text) / 4 Then
                        inserimento = True
                        For j = 1 To Len(Combo9.Text) Step 4
                            ins = False
                            rs2.MoveFirst
                            For k = 0 To rs2.RecordCount - 1
                                If Mid$(Combo9.Text, j, 3) = rs2.Fields(1).Value Then
                                    ins = True
                                    Exit For
                                End If
                                rs2.MoveNext
                            Next
                            If ins = False Then inserimento = False: Exit For
                        Next
                        If inserimento = True Then
                            ReDim Preserve vett(sel + 1)
                            vett(sel) = rs1.Fields(0).Value
                            sel = sel + 1
                        End If

                    End If
                    
                    rs1.MoveNext
                    
                Next
                stringa = ""
                For a = 0 To UBound(vett()) - 1
                    stringa = stringa & "firmware.cod_fw = '" & vett(a) & "' or "
                Next
                stringa = Left(stringa, Len(stringa) - 4)
                rs3.Open ("select firmware.cod_fw as Cod_FW, firmware.cod_vendita, firmware.nome_intero as Nome_interno, firmware.tipo_cdf, dettaglio_rif.iso_code, dettaglio_rif.versione_iso, dettaglio_rif.fitness from firmware, dettaglio_rif where (" & stringa & ") and firmware.cod_fw = dettaglio_rif.cod_fw and firmware.tipo_fw = 2 order by firmware.cod_fw"), db1, adOpenStatic, adLockReadOnly
                
         Case 8
                rs3.Open ("select distinct firmware.cod_fw as Cod_FW, firmware.cod_vendita, firmware.nome_intero as Nome_interno, firmware.tipo_cdf, dettaglio_rif.iso_code, dettaglio_rif.versione_iso, dettaglio_rif.fitness from firmware, dettaglio_rif, dettaglio_valuta, fw_mod where dettaglio_valuta.iso_code = dettaglio_rif.iso_code and firmware.cod_fw = dettaglio_rif.cod_fw and firmware.cod_fw = fw_mod.cod_fw and firmware.tipo_fw = 2 and " & conv(Combo8.ListIndex) & " = '" & Combo9.Text & "' order by firmware.cod_fw"), db1, adOpenStatic, adLockReadOnly
        End Select
    rs3.Requery
    DataGrid1.MergeCol(0) = True
    DataGrid1.MergeCol(1) = True
    DataGrid1.MergeCol(2) = True
    DataGrid1.ColWidth(0) = DataGrid1.Width * 10 / 100
    DataGrid1.ColWidth(1) = DataGrid1.Width * 10 / 100
    DataGrid1.ColWidth(2) = DataGrid1.Width * 38 / 100
    DataGrid1.ColWidth(3) = DataGrid1.Width * 10 / 100
    DataGrid1.ColWidth(4) = DataGrid1.Width * 12 / 100
    DataGrid1.ColWidth(5) = DataGrid1.Width * 8 / 100
    Case 2
        Select Case Combo8.ListIndex
            Case 0, 1, 2
            
                rs3.Open ("select firmware.cod_fw as Cod_FW, firmware.nome_intero as Nome_interno, firmware.versione, firmware.release from firmware where " & conv(Combo8.ListIndex) & " = '" & Combo9.Text & "' order by firmware.cod_fw"), db1, adOpenStatic, adLockReadOnly
        
            Case 3
            
                rs3.Open ("select firmware.cod_fw as Cod_FW, firmware.nome_intero as Nome_interno, firmware.versione, firmware.release from firmware, tbldett_fw where " & conv(Combo8.ListIndex) & " = '" & Combo9.Text & "' and firmware.cod_fw = tbldett_fw.cod_fw order by firmware.cod_fw"), db1, adOpenStatic, adLockReadOnly

            Case 4
            
                rs3.Open ("select firmware.cod_fw as Cod_FW, firmware.nome_intero as Nome_interno, firmware.versione, firmware.release from firmware, fw_mod where " & conv(Combo8.ListIndex) & " = '" & Combo9.Text & "' and firmware.cod_fw = fw_mod.cod_fw and firmware.tipo_fw=3 order by firmware.cod_fw"), db1, adOpenStatic, adLockReadOnly
        
        End Select
    rs3.Requery
    DataGrid1.ColWidth(0) = DataGrid1.Width * 10 / 100
    DataGrid1.ColWidth(1) = DataGrid1.Width * 66 / 100
    DataGrid1.ColWidth(2) = DataGrid1.Width * 10 / 100
    DataGrid1.ColWidth(3) = DataGrid1.Width * 10 / 100
    
        Case 3
        Select Case Combo8.ListIndex
            Case 0, 1, 2
            
                rs3.Open ("select firmware.cod_fw as Cod_FW, firmware.nome_intero as Nome_interno, firmware.versione, firmware.release from firmware where " & conv(Combo8.ListIndex) & " = '" & Combo9.Text & "' order by firmware.cod_fw"), db1, adOpenStatic, adLockReadOnly
        
            Case 3
            
                rs3.Open ("select firmware.cod_fw as Cod_FW, firmware.nome_intero as Nome_interno, firmware.versione, firmware.release from firmware, tbldett_fw where " & conv(Combo8.ListIndex) & " = '" & Combo9.Text & "' and firmware.cod_fw = tbldett_fw.cod_fw order by firmware.cod_fw"), db1, adOpenStatic, adLockReadOnly

            Case 4
            
                rs3.Open ("select firmware.cod_fw as Cod_FW, firmware.nome_intero as Nome_interno, firmware.versione, firmware.release from firmware, fw_mod where " & conv(Combo8.ListIndex) & " = '" & Combo9.Text & "' and firmware.cod_fw = fw_mod.cod_fw and firmware.tipo_fw=4 order by firmware.cod_fw"), db1, adOpenStatic, adLockReadOnly
        
        End Select
    rs3.Requery
    DataGrid1.ColWidth(0) = DataGrid1.Width * 10 / 100
    DataGrid1.ColWidth(1) = DataGrid1.Width * 66 / 100
    DataGrid1.ColWidth(2) = DataGrid1.Width * 10 / 100
    DataGrid1.ColWidth(3) = DataGrid1.Width * 10 / 100
        Case 4
        Select Case Combo8.ListIndex
            Case 0, 1, 2
            
                rs3.Open ("select firmware.cod_fw as Cod_FW, firmware.nome_intero as Nome_interno, firmware.versione, firmware.release from firmware where " & conv(Combo8.ListIndex) & " = '" & Combo9.Text & "' order by firmware.cod_fw"), db1, adOpenStatic, adLockReadOnly
        
            Case 3
            
                rs3.Open ("select firmware.cod_fw as Cod_FW, firmware.nome_intero as Nome_interno, firmware.versione, firmware.release from firmware, tbldett_fw where " & conv(Combo8.ListIndex) & " = '" & Combo9.Text & "' and firmware.cod_fw = tbldett_fw.cod_fw order by firmware.cod_fw"), db1, adOpenStatic, adLockReadOnly

            Case 4
            
                rs3.Open ("select firmware.cod_fw as Cod_FW, firmware.nome_intero as Nome_interno, firmware.versione, firmware.release from firmware, fw_mod where " & conv(Combo8.ListIndex) & " = '" & Combo9.Text & "' and firmware.cod_fw  = fw_mod.cod_fw and firmware.tipo_fw=5 order by firmware.cod_fw"), db1, adOpenStatic, adLockReadOnly
        
        End Select
            rs3.Requery
    DataGrid1.ColWidth(0) = DataGrid1.Width * 10 / 100
    DataGrid1.ColWidth(1) = DataGrid1.Width * 66 / 100
    DataGrid1.ColWidth(2) = DataGrid1.Width * 10 / 100
    DataGrid1.ColWidth(3) = DataGrid1.Width * 10 / 100
End Select
aa = rs3.RecordCount
If aa = 0 Then

    DataGrid1.FixedRows = 1
    DataGrid1.Rows = 2
    DataGrid1.Row = 2
    For a = 0 To DataGrid1.Cols - 1
        DataGrid1.Col = a
        DataGrid1.Text = ""
    Next

Else

    Set DataGrid1.DataSource = rs3
    DataGrid1.MergeCol(0) = True
    DataGrid1.MergeCol(1) = True
    DataGrid1.MergeCol(2) = True
    DataGrid1.MergeCol(3) = True
    DataGrid1.Visible = True
    
End If
DataGrid1.Redraw = True
DataGrid1.Refresh

Label5.Visible = True

End Sub

Private Sub Command1_Click()

End Sub

Private Sub DataGrid1_DblClick()
Dim lRet As Long
Dim sFile As String
Dim perc_rdm As String
bb = DataGrid1.Text
Select Case DataGrid1.Col
    Case 0
    Fw_Cartiglio = DataGrid1.Text
    Cartiglio_FW.Visible = True
    Cartiglio_FW.Show
    Case 1
    
    Case 2
    
    Case 3
    
    Case 4
    
    Case 5
   
    Case 6
    
    Case 7
    
    Case 8

   
    On Error GoTo aa:
   
    Case 9
    
End Select
aa:
If err.Number = 53 Or err.Number = 76 Then
Msg = "Il file non � presente"  ' Define message.
    Response = MsgBox(Msg, 0, , 8, Ctxt)
    Resume Next
End If
End Sub

Private Sub ex_Click()
    Dim cn As ADODB.Connection
    Set cn = New ADODB.Connection
    
    salva1.InitDir = "Documenti"
    salva1.Filter = "Excel file |*.xls"
    salva1.Flags = cdlOFNOverwritePrompt Or cdlOFNPathMustExist
    salva1.Filename = ""
    salva1.ShowSave
    DoEvents
    
    If salva1.Filename <> "" Then
        File_excel = salva1.Filename
    Else
        Exit Sub
    End If

    If Dir(File_excel) <> "" Then 'elimino il file se esiste altrimenti va in errore
    
        Kill File_excel
    
    End If
    
    cn.ConnectionString = db1.ConnectionString
    cn.Open
    Dim strSQL As String
    If Len(rs3.Source) > 1 Then
    For a = 1 To Len(rs3.Source)
        If UCase(Mid$(rs3.Source, a, 4)) = "FROM" Then
            aa = Right$(rs3.Source, (Len(rs3.Source) - a) + 2)
            bb = UCase(Mid$(rs3.Source, a, 4))
            strSQL = Mid$(rs3.Source, 1, a - 1) & "INTO [Excel 8.0;Database=" & File_excel & "].[Foglio1]" & Right$(rs3.Source, (Len(rs3.Source) - a) + 2)
            Exit For
        End If
    Next
    

    
   cn.Execute strSQL
    End If
    cn.Close
    
End Sub

Private Sub Form_Activate()
On Error GoTo err
If disposizione_dettaglio = False Then
DataGrid1.Visible = False
disposizione_dettaglio = True
    For Each obj In Dettaglio_FW
    
        
        obj.Visible = False
        obj.Height = obj.Height * Scala
        obj.Width = obj.Width * Scala
        obj.Top = obj.Top * Scala
        obj.Left = obj.Left * Scala
        obj.Font = "Arial"
    Next
    'dataGrid1.HeadFont.Size = 10
    'dataGrid1.HeadFont.Bold = True
    aa = Read_Set("Server", "databasePath")
    aa = Left(aa, Len(aa) - 1)
    
    Set db1 = New ADODB.Connection
    'Set db2 = New ADODB.Connection
    Set rs1 = New ADODB.Recordset
    Set rs2 = New ADODB.Recordset
    Set rs3 = New ADODB.Recordset
    Set rs4 = New ADODB.Recordset

    db1.Open "Provider =Microsoft.Jet.OLEDB.4.0;Data Source=" & aa & ";"
    'db2.Open "Provider =Microsoft.Jet.OLEDB.4.0;Data Source=" & aa & ";"
    
    rs1.CursorLocation = adUseClient
    rs2.CursorLocation = adUseClient
    rs3.CursorLocation = adUseClient
    rs4.CursorLocation = adUseClient
    
    rs1.Open ("SELECT tipo_fw.descrizione from tipo_fw"), db1, adOpenStatic, adLockReadOnly
    rs4.Open ("select dettaglio_valuta.iso_code, dettaglio_valuta.paese, dettaglio_valuta.valuta from dettaglio_valuta order by dettaglio_valuta.iso_code"), db1, adOpenStatic, adLockReadOnly
    aa = rs4.RecordCount
    Set DataGrid1.DataSource = rs3
    'dataGrid1.AllowArrows = False
    'dataGrid1.AllowRowSizing = False
    'dataGrid1.ColumnHeaders = True
    DataGrid1.Refresh
    DataGrid1.Left = 100
    DataGrid1.Height = Screen.Height - DataGrid1.Top - 1000
    DataGrid1.Width = Screen.Width - 8000 * Scala
    
    Set DataGrid2.DataSource = rs4
    DataGrid2.AllowArrows = False
    DataGrid2.AllowRowSizing = False
    DataGrid2.ColumnHeaders = True
    DataGrid2.Refresh
    DataGrid2.Left = DataGrid1.Left + DataGrid1.Width + 500
    DataGrid2.Height = Screen.Height - DataGrid2.Top - 1000
    DataGrid2.Width = Screen.Width - DataGrid2.Left - 300
    'DataGrid1.Top = 2000
    rs4.Requery
    DataGrid2.Columns.Item(0).Width = DataGrid2.Width * 15 / 100
    DataGrid2.Columns.Item(1).Width = DataGrid2.Width * 25 / 100
    DataGrid2.Columns.Item(2).Width = DataGrid2.Width * 55 / 100
    
    DataGrid1.Font.Size = 9
    DataGrid1.Font.Bold = False
    'For a = 1 To dataGrid1.Columns.Count - 1
    'dataGrid1.Columns.Item(a).AllowSizing = True
    'dataGrid1.Columns.Item(a).Alignment = dbgLeft
    'Next
    Label4.Top = MSFlexGrid1Top - Label4.Height - 50
    Frame1.Top = 500 * Scala
    Frame1.Left = 500 * Scala '(Screen.Width - Frame1.Width) / 2
    Label4.Caption = "Connessione al database in corso"
    Image3.Top = 500 * Scala
    Image3.Width = 2325
    Image3.Height = 630
    Image3.Left = Screen.Width - 400 - Image3.Width
    
    'Label3.Caption = "ARCHIVIO FILES UFFICIALI"
    Label3.Top = 1000 * Scala
    Label3.Left = Frame1.Left + Frame1.Width + (2000 * Scala)

    Label5.Left = DataGrid1.Left + 400
    Label5.Height = DataGrid1.Top - Label5.Top

    For a = 0 To rs1.RecordCount - 1
            On Error Resume Next
    
            Combo1.AddItem rs1.Fields(0).Value
            rs1.MoveNext
    Next
     For Each obj In Dettaglio_FW
    
       
        obj.Visible = True
        
    Next
    Label5.Visible = False
    DataGrid1.Visible = False
    DataGrid2.Visible = False
    End If

aa:
Exit Sub
err:


Select Case err.Number   ' Evaluate error number.
      Case 339   ' "File already open" error.
        Response = MsgBox("Files mancanti, � necessario avviare l'installazione,il software verr� chiuso, procedere con l'installazione, al termine sar� possibile avviare nuovamente il software", 0, "Avviso", help, Ctxt)
        aa = Shell("\\Cashprosrv\ARCHIVIO\SOFTWARE\SW_FILES\SW_UFFICIALI\Database_fw\Setup_dll.exe", vbNormalFocus)
        End
      Case Else
         Resume Next
End Select


End Sub

Function conv(aa)
Select Case Combo1.ListIndex
    Case 0, 2, 3, 4
        Select Case aa
            Case 0
                conv = "firmware.cod_fw"
            Case 1
                conv = "firmware.nome_intero"
            Case 2
                conv = "Firmware.release"
            Case 3
                conv = "tbldett_fw.nome_int"
            Case 4
                conv = "fw_mod.id_modulo"
                
        End Select
    Case 1
        Select Case aa
            Case 0
                conv = "firmware.cod_fw"
            Case 1
                conv = "firmware.cod_vendita"
            Case 2
                conv = "firmware.nome_intero"
            Case 3
                conv = "Firmware.tipo_cdf"
            Case 4, 5
                conv = "dettaglio_rif.iso_code"
            Case 6
                conv = "dettaglio_valuta.paese"
            Case 7
                conv = "dettaglio_valuta.valuta"
            Case 8
                conv = "fw_mod.id_modulo"
        End Select
    Case 2
    
    Case 3
End Select
End Function

Private Sub Form_Terminate()
    disposizione_dettaglio = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Form1.Enabled = True
    disposizione_dettaglio = False
End Sub

Private Sub SQL_Click()
    Advanced.Visible = True
End Sub

Private Sub sta_Click()
    Set Griglia_da_stampare = DataGrid1
    frmPRINT.Visible = True
End Sub

Private Sub ultima_Click()
    Nuova_versione.Visible = True
End Sub

Private Sub Versione_Click()
    frmAbout.Visible = True
End Sub
