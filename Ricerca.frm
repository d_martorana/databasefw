VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form Ricerca 
   BackColor       =   &H00E0E0E0&
   Caption         =   "Ricerca prodotti - Database FW"
   ClientHeight    =   11310
   ClientLeft      =   465
   ClientTop       =   1050
   ClientWidth     =   14505
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   13.5
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   ScaleHeight     =   11310
   ScaleWidth      =   14505
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHFlexGrid1 
      CausesValidation=   0   'False
      Height          =   5295
      Left            =   360
      TabIndex        =   9
      Top             =   3120
      Width           =   12360
      _ExtentX        =   21802
      _ExtentY        =   9340
      _Version        =   393216
      Cols            =   1
      FixedCols       =   0
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLines       =   2
      GridLinesUnpopulated=   2
      MergeCells      =   4
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   1
   End
   Begin VB.Frame tua_stringa_query 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1875
      Left            =   150
      TabIndex        =   0
      Top             =   270
      Width           =   9330
      Begin VB.ComboBox Combo2 
         BeginProperty Font 
            Name            =   "Comic Sans MS"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3150
         OLEDropMode     =   1  'Manual
         Sorted          =   -1  'True
         TabIndex        =   2
         Top             =   690
         Width           =   3285
      End
      Begin VB.ComboBox Combo1 
         BeginProperty Font 
            Name            =   "Comic Sans MS"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   240
         TabIndex        =   1
         Top             =   690
         Width           =   2490
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Search key"
         BeginProperty Font 
            Name            =   "Comic Sans MS"
            Size            =   10.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   3150
         TabIndex        =   4
         Top             =   330
         Width           =   2175
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Search by"
         BeginProperty Font 
            Name            =   "Comic Sans MS"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   255
         TabIndex        =   3
         Top             =   330
         Width           =   2295
      End
   End
   Begin MSComDlg.CommonDialog Salva1 
      Left            =   1080
      Top             =   8775
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Double click for details"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   510
      Left            =   11085
      TabIndex        =   11
      Top             =   2610
      Width           =   1485
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Double click for details"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   510
      Left            =   3945
      TabIndex        =   10
      Top             =   2595
      Width           =   1485
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "Double click for details"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   510
      Left            =   8340
      TabIndex        =   8
      Top             =   2610
      Width           =   1485
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "Double click for details"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   450
      Left            =   915
      TabIndex        =   7
      Top             =   2595
      Width           =   1485
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Height          =   450
      Left            =   330
      TabIndex        =   6
      Top             =   2430
      Width           =   8775
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "SEARCH BY PRODUCT CODE"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1500
      Left            =   9735
      TabIndex        =   5
      Top             =   1350
      Width           =   4725
   End
   Begin VB.Image Image3 
      Height          =   630
      Left            =   12225
      Picture         =   "Ricerca.frx":0000
      Top             =   240
      Width           =   2325
   End
   Begin VB.Menu f 
      Caption         =   "File"
      Begin VB.Menu Stampa 
         Caption         =   "Print"
      End
      Begin VB.Menu Excel 
         Caption         =   "Save as Excel format"
      End
   End
   Begin VB.Menu Ri 
      Caption         =   "Search"
      Begin VB.Menu SQL 
         Caption         =   "Advanced SQL search"
      End
   End
   Begin VB.Menu He 
      Caption         =   "Help"
      Begin VB.Menu ver 
         Caption         =   "Version"
      End
      Begin VB.Menu UM 
         Caption         =   "Last modify"
      End
   End
End
Attribute VB_Name = "Ricerca"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public rs2 As ADODB.Recordset
Public rs3 As ADODB.Recordset

Private Sub Combo1_Change()
    Dim trovato As Boolean
    trovato = False
    For i = 0 To Combo1.ListCount - 1
        If Combo1.List(i) = Combo1.Text Then trovato = True
    Next
    If trovato = True Then
        Combo2.Clear
        'Call res
        'DataGrid1.Enabled = False
        
        If Combo1.Text <> "Nome_Interno" Then
        
            On Error Resume Next
        
            rs2.Close
            rs2.Open ("SELECT DISTINCT " & conv(Combo1.Text) & " FROM " & SEL_TAB(Combo1.Text)), db1, adOpenStatic, adLockReadOnly
            rs2.Requery
            aa = rs2.RecordCount
            rs2.MoveFirst
            aa = (rs2.Fields(0).Name)
            
            Do While Not rs2.EOF
            If rs2.Fields(0).Value <> "Null" Then Combo2.AddItem rs2.Fields(0).Value
            
            rs2.MoveNext
            
            Loop
        Else
         
            On Error Resume Next
            Insert = True
            rs2.Close
            rs2.Open ("SELECT DISTINCT Nome_intero from firmware, tblman where tblman.cod_FW = Firmware.cod_fw"), db1, adOpenStatic, adLockReadOnly
            rs2.Requery
            aa = rs2.RecordCount
            rs2.MoveFirst
            aa = (rs2.Fields(0).Name)
            
            Do While Not rs2.EOF
            If rs2.Fields(0).Value <> "Null" Then Combo2.AddItem rs2.Fields(0).Value
            
            rs2.MoveNext
            
            Loop
        
        End If
        
    End If
    'DataGrid1.Enabled = True

End Sub




Private Sub Combo1_Click()
    If Combo1.Text <> "" Then
        Combo2.Clear
        
        If Combo1.Text <> "Nome_Interno" Then
        
            On Error Resume Next
        
            rs2.Close
            rs2.Open ("SELECT DISTINCT " & conv(Combo1.Text) & " FROM " & SEL_TAB(Combo1.Text)), db1, adOpenStatic, adLockReadOnly
            rs2.Requery
            aa = rs2.RecordCount
            rs2.MoveFirst
            aa = (rs2.Fields(0).Name)
            
            Do While Not rs2.EOF
            If rs2.Fields(0).Value <> "Null" Then
                If rs2.Fields(0).Value <> "CC3R" Then Combo2.AddItem rs2.Fields(0).Value
            End If
            
            rs2.MoveNext
            
            Loop
        Else
        
        
         
            On Error Resume Next
            Insert = True
            rs2.Close
            rs2.Open ("SELECT DISTINCT Nome_intero from firmware, tblman where tblman.cod_FW = Firmware.cod_fw"), db1, adOpenStatic, adLockReadOnly
            rs2.Requery
            aa = rs2.RecordCount
            rs2.MoveFirst
            aa = (rs2.Fields(0).Name)
            
            Do While Not rs2.EOF
            If rs2.Fields(0).Value <> "Null" Then Combo2.AddItem rs2.Fields(0).Value
            
            rs2.MoveNext
            
            Loop
        
        End If
    
    End If

End Sub

Private Sub Combo2_Change()
    If Combo1.Text <> "" Then
    Dim aa As String
    On Error Resume Next
    If Combo2.Text = "" Then
    rs1.Close
    If UCase(Combo2.Text) = "SPAREBOARD" Then
    
    Else
        rs1.Open ("SELECT prodotti.categoria_prodotto AS Cat_Prod, clienti.nome_cliente AS Cliente, tblman.ID_Prodotto AS Prodotto, tblman.id_modulo AS Cod_Modulo, moduli.descrizione AS Descrizione_Modulo, tblman.cod_fw AS Cod_Firmware, tipo_fw.descrizione AS Tipo_FW, tblman.id_nome_unit_config as Unit_Config, tblman.Banco, tblman.last_rdm AS Ultima_ECN, tblman.boardsetup AS Setup_Piastra FROM tblman, clienti, moduli, tipo_fw, prodotti WHERE tblman.id_cliente = clienti.id_cliente AND tblman.id_modulo = moduli.id_modulo AND tblman.tipo_fw = tipo_fw.tipo_fw AND tblman.id_prodotto = prodotti.id_prodotto ORDER BY prodotti.categoria_prodotto, tblman.ID_Prodotto, tblman.id_cliente ASC, tblman.Tipo_fw, tblman.banco asc"), db1, adOpenStatic, adLockReadOnly
    End If
    
    
    Else
        If Combo1.Text <> "Nome_Interno" Then
            aa = Combo2.Text
        Else
            rs2.Close
            rs2.Open ("SELECT Firmware.cod_fw FROM Firmware WHERE Firmware.nome_intero = '" & Combo2.Text & "'"), db1, adOpenStatic, adLockReadOnly
            rs2.Requery
            aa = (rs2.Fields("cod_fw"))
        End If
    
    rs1.Close
    kk = "SELECT tblman.ID_Prodotto as Prodotto, prodotti.categoria_prodotto as Cat_Prod, clienti.nome_cliente as Cliente, tblman.id_modulo as Cod_Modulo, moduli.descrizione as Descrizione_Modulo, tblman.cod_fw as Cod_Firmware, tipo_fw.descrizione as Tipo_FW, tblman.Banco, tblman.last_rdm as Ultima_ECN from tblman, clienti, moduli, tipo_fw, prodotti where " & conv(Combo1.Text) & " like '" & aa & "' and tblman.id_cliente = clienti.id_cliente AND tblman.id_modulo = moduli.id_modulo and tblman.tipo_fw = tipo_fw.tipo_fw and tblman.id_prodotto = prodotti.id_prodotto order by prodotti.categoria_prodotto, tblman.ID_Prodotto, tblman.id_cliente asc, tblman.Tipo_fw, tblman.banco asc"
    
    Call Compila_griglia(Ordine_visu(Combo1.Text), Ordine_alfa(Combo1.Text), Combo1.Text, aa)
    
    
    End If
    
    dddd = rs1.RecordCount
    
    End If
End Sub

Private Sub Combo2_Click()
    If Combo1.Text <> "" Then
        Dim aa As String
        On Error Resume Next
        If Combo2.Text = "" Then
            rs1.Close
            rs1.Open ("SELECT prodotti.categoria_prodotto AS Cat_Prod, clienti.nome_cliente AS Cliente, tblman.ID_Prodotto AS Prodotto, tblman.id_modulo AS Cod_Modulo, moduli.descrizione AS Descrizione_Modulo, tblman.cod_fw AS Cod_Firmware, tipo_fw.descrizione AS Tipo_FW, tblman.Banco, tblman.last_rdm AS Ultima_ECN FROM tblman, clienti, moduli, tipo_fw, prodotti WHERE tblman.id_cliente = clienti.id_cliente AND tblman.id_modulo = moduli.id_modulo AND tblman.tipo_fw = tipo_fw.tipo_fw AND tblman.id_prodotto = prodotti.id_prodotto ORDER BY prodotti.categoria_prodotto, tblman.ID_Prodotto, tblman.id_cliente asc, tblman.Tipo_fw, tblman.banco asc"), db1, adOpenStatic, adLockReadOnly
        Else
            If Combo1.Text <> "Nome_Interno" Then
                aa = Combo2.Text
            Else
                rs2.Close
                rs2.Open ("SELECT Firmware.cod_fw FROM Firmware where Firmware.nome_intero = '" & Combo2.Text & "'"), db1, adOpenStatic, adLockReadOnly
                rs2.Requery
                aa = (rs2.Fields("cod_fw"))
            End If
            rs1.Close
            kk = "SELECT tblman.ID_Prodotto AS Prodotto, prodotti.categoria_prodotto AS Cat_Prod, clienti.nome_cliente AS Cliente, tblman.id_modulo AS Cod_Modulo, moduli.descrizione AS Descrizione_Modulo, tblman.cod_fw AS Cod_Firmware, tipo_fw.descrizione AS Tipo_FW, tblman.id_nome_unit_config AS Unit_Config, tblman.Banco, tblman.last_rdm AS Ultima_ECN, tblman.boardsetup as Setup_Piastra FROM tblman, clienti, moduli, tipo_fw, prodotti WHERE " & conv(Combo1.Text) & " LIKE '" & aa & "' AND tblman.id_cliente = clienti.id_cliente AND tblman.id_modulo = moduli.id_modulo AND tblman.tipo_fw = tipo_fw.tipo_fw AND tblman.id_prodotto = prodotti.id_prodotto ORDER BY prodotti.categoria_prodotto, tblman.ID_Prodotto, tblman.id_cliente asc, tblman.Tipo_fw, tblman.banco ASC"
            Call Compila_griglia(Ordine_visu(Combo1.Text), Ordine_alfa(Combo1.Text), Combo1.Text, aa)
        End If
        dddd = rs1.RecordCount
    End If
End Sub



Private Sub Command1_Click()
'AA = MSHFlexGrid1.Height
'bb = MSHFlexGrid1.Width
'MSHFlexGrid1.Height = 30000
'MSHFlexGrid1.Width = 30000
'Printer.PaintPicture MSHFlexGrid1.Picture, 0, 0, 30000, 30000
'Printer.EndDoc
'MSHFlexGrid1.Height = AA
'MSHFlexGrid1.Width = bb
''MSHFlexGrid1.Picture , 0, 0




End Sub

Private Sub Command2_Click()
'MSHFlexGrid1.GridLines = 1 'RGB(255, 44, 22)
'MSHFlexGrid1.GridLineWidthIndent = 3 'RGB(255, 44, 22)
MSHFlexGrid1.Row = 1
MSHFlexGrid1.Col = 2
MSHFlexGrid1.CellBackColor = RGB(255, 255, 0)
MSHFlexGrid1.Row = 1
MSHFlexGrid1.Col = 3
MSHFlexGrid1.CellBackColor = 55
MSHFlexGrid1.Refresh




End Sub

Private Sub excel_Click()
    Dim cn As ADODB.Connection
    Set cn = New ADODB.Connection
    
    salva1.InitDir = "Documenti"
    salva1.Filter = "Excel file |*.xls"
    salva1.Flags = cdlOFNOverwritePrompt Or cdlOFNPathMustExist
    salva1.Filename = ""
    salva1.ShowSave
    DoEvents
    
    If salva1.Filename <> "" Then
        File_excel = salva1.Filename
    Else
        Exit Sub
    End If

    If Dir(File_excel) <> "" Then 'elimino il file se esiste altrimenti va in errore
    
        Kill File_excel
    
    End If
    
    cn.ConnectionString = db1.ConnectionString
    cn.Open
    Dim strSQL As String
    For a = 1 To Len(rs1.Source)
        If UCase(Mid$(rs1.Source, a, 4)) = "FROM" Then
            aa = Right$(rs1.Source, (Len(rs1.Source) - a) + 2)
            bb = UCase(Mid$(rs1.Source, a, 4))
            strSQL = Mid$(rs1.Source, 1, a - 1) & "INTO [Excel 8.0;Database=" & File_excel & "].[Foglio1]" & Right$(rs1.Source, (Len(rs1.Source) - a) + 2)
            Exit For
        End If
    Next
    

    
   cn.Execute strSQL
    cn.Close

End Sub

Private Sub Form_Activate()
    On Error GoTo err
    If disposizione_ricerca = False Then
    disposizione_ricerca = True
        For Each obj In Ricerca
            obj.Visible = False
            obj.Height = obj.Height * Scala
            obj.Width = obj.Width * Scala
            obj.Top = obj.Top * Scala
            obj.Left = obj.Left * Scala
            obj.Font = "Arial"
        Next
'        aa = Read_Set("Server", "databasePath")
'        aa = Left(aa, Len(aa) - 1)
        aa = "c:\database_fw\man1220_dati.mdb"
        Set db1 = New ADODB.Connection
        Set rs1 = New ADODB.Recordset
        Set rs2 = New ADODB.Recordset
        Set rs3 = New ADODB.Recordset
        db1.Open "Provider =Microsoft.Jet.OLEDB.4.0;Data Source=" & aa & ";"
        rs1.CursorLocation = adUseClient
        rs2.CursorLocation = adUseClient
        rs3.CursorLocation = adUseClient
        rs1.Open ("SELECT prodotti.categoria_prodotto AS Cat_Prod, clienti.nome_cliente AS Cliente, tblman.ID_Prodotto AS Cod_Prodotto, tblman.id_modulo AS Cod_Modulo, moduli.descrizione AS Descrizione_Modulo, tblman.cod_fw AS Cod_Firmware, tblman.cod_fw_field AS Cod_Firmware_Field, tipo_fw.descrizione AS Tipo_FW, tblman.id_nome_unit_config AS Unit_Config, tblman.Banco, tblman.last_rdm AS Ultima_ECN, tblman.boardsetup AS Setup_Piastra FROM tblman, clienti, moduli, tipo_fw, prodotti WHERE tblman.id_cliente = clienti.id_cliente AND tblman.id_modulo = moduli.id_modulo AND tblman.tipo_fw = tipo_fw.tipo_fw AND tblman.id_prodotto = prodotti.id_prodotto ORDER BY prodotti.categoria_prodotto, tblman.id_cliente ASC, tblman.ID_Prodotto, tblman.id_modulo, tblman.Tipo_fw, tblman.banco ASC"), db1, adOpenStatic, adLockReadOnly
        rs1.Requery
        aa = rs1.RecordCount
        If aa < 2 Then
            msDELAY (0.5)
            rs1.Requery
        End If
        aa = rs1.RecordCount
        Set MSHFlexGrid1.DataSource = rs1
        MSHFlexGrid1.Left = 100
        MSHFlexGrid1.Height = Screen.Height - MSHFlexGrid1.Top - 1000
        MSHFlexGrid1.Width = Screen.Width - 400
        MSHFlexGrid1.FontBand(0).Bold = True
        MSHFlexGrid1.Font.Size = 9
        MSHFlexGrid1.Font.Bold = False
        Label6.Left = MSHFlexGrid1.Width * 66 / 100 + 410
        Label6.Height = MSHFlexGrid1.Top - Label5.Top
        Label5.Left = MSHFlexGrid1.Left + 400
        Label5.Height = MSHFlexGrid1.Top - Label5.Top
    
        Label4.Top = MSFlexGrid1.Top - Label4.Height - 50
        Frame1.Top = 500 * Scala
        Frame1.Left = 500 * Scala '(Screen.Width - Frame1.Width) / 2
        Label4.Caption = "Connessione al database in corso"
        Image3.Top = 500 * Scala
        Image3.Width = 2325
        Image3.Height = 630
        Image3.Left = Screen.Width - 400 - Image3.Width
        
        'Label3.Caption = "RICERCA PER ASSOCIAZIONE AL PRODOTTO"
        Label3.Top = 1000 * Scala
        Label3.Left = Frame1.Left + Frame1.Width + (1500 * Scala)
        Label4.Caption = ""
    
       
    For a = 0 To rs1.Fields.Count - 1
        On Error Resume Next
        If rs1.Fields(a).Name <> "Setup_Piastra" Then
            Combo1.AddItem rs1.Fields(a).Name
        End If
        If rs1.Fields(a).Name = "Cod_Firmware" Then
           Combo1.AddItem "Nome_Interno"
        End If
    Next
     For Each obj In Ricerca
        
            On Error Resume Next
            obj.Visible = True
    
        Next
    
    MSHFlexGrid1.MergeCells = flexMergeFree
    MSHFlexGrid1.MergeCol(0) = True
    MSHFlexGrid1.MergeCol(1) = True
    MSHFlexGrid1.MergeCol(2) = True
    MSHFlexGrid1.MergeCol(3) = True
    MSHFlexGrid1.MergeCol(4) = True
    Call Dimensionamento_gliglia
    
    End If
    Exit Sub
err:
    
    Select Case err.Number   ' Evaluate error number.
          Case 339   ' "File already open" error.
            Response = MsgBox("Files mancanti, � necessario avviare l'installazione,il software verr� chiuso, procedere con l'installazione, al termine sar� possibile avviare nuovamente il software", 0, "Avviso", help, Ctxt)
            aa = Shell("\\Cashprosrv\ARCHIVIO\SOFTWARE\SW_FILES\SW_UFFICIALI\Database_fw\Setup_dll.exe", vbNormalFocus)
            End
          Case Else
             Resume Next
    End Select


End Sub

Private Sub Form_Load()
    On Error GoTo error
    WorkDir = App.Path
    
    If Mid$(WorkDir, Len(WorkDir), 1) <> "\" Then
        WorkDir = WorkDir + "\"
    End If
    
    Scala = Screen.Height / 14400
    
    
    
    'Data1.Recordset.MoveFirst
    
    Combo1.Clear
    Combo2.Clear
    FileCopy "\\arcadata\Archive\FIRMWARES\DATABASE_FW\man1220_dati.mdb", "c:\database_fw\man1220_dati.mdb"
error:
    Select Case err.Number
        Case 0
        Case 70
            MsgBox "Non � possibile aggiornare il database, verr� utilizzato l'ultimo aggiornamento ricevuto"
        Case Else
            MsgBox err.Number & " - " & err.Description
    End Select
End Sub

Function SEL_TAB(aa)
Select Case aa
    Case "Cod_Prodotto"
        SEL_TAB = "tblman"
    Case "Cat_Prod"
        SEL_TAB = "prodotti"
    Case "Cliente"
        SEL_TAB = "clienti"
    Case "Cod_Modulo"
        SEL_TAB = "tblman"
    Case "Descrizione_Modulo"
        SEL_TAB = "moduli"
    Case "Cod_Firmware", "Cod_Firmware_Field"
        SEL_TAB = "tblman"
    Case "Tipo_FW"
        SEL_TAB = "tipo_fw"
    Case "Ultima_ECN"
        SEL_TAB = "tblman"
    Case "Banco"
        SEL_TAB = "tblman"
    Case "Setup_Piastra"
        SEL_TAB = "tblman"
End Select
End Function

Function conv(aa As String) As String

    Select Case aa
        Case "Cod_Prodotto"
            conv = "tblman.ID_Prodotto"
        Case "Cat_Prod"
            conv = "prodotti.categoria_prodotto"
        Case "Cliente"
            conv = "clienti.nome_cliente"
        Case "Cod_Modulo"
            conv = "tblman.id_modulo"
        Case "Descrizione_Modulo"
            conv = "moduli.descrizione"
        Case "Cod_Firmware", "Nome_Interno"
            conv = "tblman.cod_fw"
        Case "Tipo_FW"
            conv = "tipo_fw.descrizione"
        Case "Ultima_ECN"
            conv = "tblman.last_rdm"
        Case "Banco"
            conv = "tblman.banco"
        Case "Cod_Firmware_Field"
            conv = "tblman.cod_fw_field"
        Case "Setup_Piastra"
            conv = "tblman.boardsetup"
    End Select
End Function

Function conv_num(num As Integer)
Select Case num
    Case 0
    conv_num = "tblman.ID_Prodotto"
    Case 1
    conv_num = "prodotti.categoria_prodotto"
    Case 2
    conv_num = "clienti.nome_cliente"
    Case 3
    conv_num = "tblman.id_modulo"
    Case 4
    conv_num = "moduli.descrizione"
    Case 5
    conv_num = "tblman.cod_fw"
    Case 6
    conv_num = "tipo_fw.descrizione"
    Case 7
    conv_num = "tblman.last_rdm"
    Case 8
    conv_num = "tblman.banco"
    
End Select
End Function

Private Sub Form_Resize()
    'Call Dimensionamento_gliglia
End Sub

Private Sub Form_Terminate()
    disposizione_ricerca = False
End Sub


Private Sub Form_Unload(Cancel As Integer)
    Form1.Enabled = True
    disposizione_ricerca = False
End Sub



Private Sub MSHFlexGrid1_Click()
    aa = MSHFlexGrid1.MouseRow
End Sub

Private Sub MSHFlexGrid1_DblClick()
    Dim kk As String
    Dim aa As String
    If MSHFlexGrid1.MouseRow = 0 Then
    
    MSHFlexGrid1.Row = 0
    kk = MSHFlexGrid1.Text
        
        
    On Error Resume Next
    If Combo2.Text = "" Then
    rs1.Close
    
    
    MSHFlexGrid1.Clear
    'rs1.Open ("SELECT prodotti.categoria_prodotto as Cat_Prod, clienti.nome_cliente as Cliente, tblman.ID_Prodotto as Prod, tblman.id_modulo as Cod_Modulo, moduli.descrizione as Descrizione_Modulo, tblman.cod_fw as Cod_Firmware, tipo_fw.descrizione as Tipo_FW, tblman.Banco, tblman.last_rdm as Ultima_ECN from tblman, clienti, moduli, tipo_fw, prodotti where tblman.id_cliente = clienti.id_cliente AND tblman.id_modulo = moduli.id_modulo and tblman.tipo_fw = tipo_fw.tipo_fw and tblman.id_prodotto = prodotti.id_prodotto order by " & conv(kk)), db1, adOpenStatic, adLockReadOnly
    Call Compila_griglia("prodotti.categoria_prodotto as Cat_Prod, clienti.nome_cliente as Cliente, tblman.ID_Prodotto as Prodotto, tblman.id_modulo as Cod_Modulo, moduli.descrizione as Descrizione_Modulo, tblman.cod_fw as Cod_Firmware, tblman.cod_fw_field as Cod_Firmware_Field, tipo_fw.descrizione as Tipo_FW, tblman.Banco, tblman.last_rdm as Ultima_ECN", ordinamento(kk), "", "")
    Else
        If Combo1.Text <> "Nome_Interno" Then
            aa = Combo2.Text
        Else
            rs2.Close
            rs2.Open ("select Firmware.cod_fw from Firmware where Firmware.nome_intero = '" & Combo2.Text & "'"), db1, adOpenStatic, adLockReadOnly
            rs2.Requery
            aa = (rs2.Fields("cod_fw"))
        End If
    
    'rs1..RecordSource = ("SELECT tblman.ID_Prodotto as Prodotto, prodotti.categoria_prodotto as Cat_Prodotto, clienti.nome_cliente as Cliente, tblman.id_modulo as Cod_Modulo, moduli.descrizione as Descrizione_Modulo, tblman.cod_fw as Cod_Firmware, tipo_fw.descrizione as Tipo_FW, tblman.Banco, tblman.last_rdm as Ultima_ECN from tblman, clienti, moduli, tipo_fw, prodotti where " & conv(Combo1.Text) & " like '" & aa & "' and tblman.id_cliente = clienti.id_cliente AND tblman.id_modulo = moduli.id_modulo and tblman.tipo_fw = tipo_fw.tipo_fw and tblman.id_prodotto = prodotti.id_prodotto order by prodotti.categoria_prodotto, tblman.ID_Prodotto, tblman.id_cliente asc, tblman.Tipo_fw, tblman.banco asc")
    rs1.Close
    'kk = Ordine_visu(Combo1.Text) & "     " & Ordine_alfa(conv(kk)) & "     " & Combo1.Text & "     " & aa
    'rs1.Open ("SELECT prodotti.categoria_prodotto as Cat_Prodotto, clienti.nome_cliente as Cliente, tblman.ID_Prodotto as Prodotto, tblman.id_modulo as Cod_Modulo, moduli.descrizione as Descrizione_Modulo, tblman.cod_fw as Cod_Firmware, tipo_fw.descrizione as Tipo_FW, tblman.Banco, tblman.last_rdm as Ultima_ECN from tblman, clienti, moduli, tipo_fw, prodotti where " & conv(Combo1.Text) & " = '" & aa & "' and tblman.id_cliente = clienti.id_cliente AND tblman.id_modulo = moduli.id_modulo and tblman.tipo_fw = tipo_fw.tipo_fw and tblman.id_prodotto = prodotti.id_prodotto order by " & conv(kk) & " ASC;"), db1, adOpenStatic, adLockReadOnly
    Call Compila_griglia(Ordine_visu(Combo1.Text), ordinamento(kk), Combo1.Text, aa)
    
    aa = (rs1.Fields(0).Value)
    End If
    rs1.Requery
    Set MSHFlexGrid1.DataSource = rs1
    hdhd = rs1.RecordCount
    Else
    
    Select Case rs1.Fields(MSHFlexGrid1.Col).Name
        Case "Unit_Config"
            UnitConfig.idConfig = MSHFlexGrid1.Text
            UnitConfig.Show
        Case "Cod_Prodotto"
            Dettaglio_prodotto = MSHFlexGrid1.Text
            'Prodotto_Dettaglio.Visible = True
            'Prodotto_Dettaglio.Show
         
        Case "Cod_Firmware", "Cod_Firmware_Field"
            Fw_Cartiglio = MSHFlexGrid1.Text
            Cartiglio_FW.Visible = True
            Cartiglio_FW.Show
        
        Case "Ultima_ECN"
            perc_rdm = Read_Set("Server", "rdm_path")
            perc_rdm = Left(perc_rdm, Len(perc_rdm) - 1)
            sFile = perc_rdm & MSHFlexGrid1.Text & ".doc"
            If Dir(sFile) = MSHFlexGrid1.Text & ".doc" Then
                lRet = ShellExecute(0, "open", sFile, "", "", 1)
            Else
                sFile = perc_rdm & MSHFlexGrid1.Text & "_A.doc"
                If Dir(sFile) = MSHFlexGrid1.Text & "_A.doc" Then
                    
                    lRet = ShellExecute(0, "open", sFile, "", "", 1)
                End If
            End If
        Case "Setup_Piastra"
            Dim filePdf As String
            filePdf = Read_Set("Server", "setupboard")
            filePdf = Left(filePdf, Len(filePdf) - 1)
            sFile = filePdf & MSHFlexGrid1.Text & ".pdf"
            'If Dir(sFile) = MSHFlexGrid1.Text & ".pdf" Then
                lRet = ShellExecute(0, "open", sFile, "", "", 1)
            'Else
            '    sFile = filePdf & MSHFlexGrid1.Text & "_A.pdf"
            '    If Dir(sFile) = MSHFlexGrid1.Text & "_A.doc" Then
            '
            '        lRet = ShellExecute(0, "open", sFile, "", "", 1)
            '    End If
            'End If
        On Error GoTo aa:
       
    
        
    End Select
    End If
    Exit Sub
aa:
    If err.Number = 53 Or err.Number = 76 Then
    Msg = "Il file non � presente"  ' Define message.
        Response = MsgBox(Msg, 0, , 8, Ctxt)
        Resume Next
    End If
End Sub

Private Sub MSHFlexGrid1_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
    If MSHFlexGrid1.MouseRow = 0 Then
        MSHFlexGrid1.MousePointer = 5
    Else
        MSHFlexGrid1.MousePointer = Default
    End If
End Sub


Sub Compila_griglia(ordine_visualizzazione As String, ordine_alfabetico As String, ricerca_per As String, chiave_ricerca As String)
    On Error GoTo aa:
    MSHFlexGrid1.Visible = False
    MSHFlexGrid1.ClearStructure
    MSHFlexGrid1.Clear
    rs1.Close
    kk = "SELECT " & ordine_visualizzazione & " from tblman, clienti, moduli, tipo_fw, prodotti where " & conv(ricerca_per) & " = '" & chiave_ricerca & "' and tblman.id_cliente = clienti.id_cliente AND tblman.id_modulo = moduli.id_modulo and tblman.tipo_fw = tipo_fw.tipo_fw and tblman.id_prodotto = prodotti.id_prodotto order by " & ordine_alfabetico
    If ricerca_per <> "" Then
        rs1.Open ("SELECT " & ordine_visualizzazione & " FROM tblman, clienti, moduli, tipo_fw, prodotti where " & conv(ricerca_per) & " = '" & chiave_ricerca & "' and tblman.id_cliente = clienti.id_cliente AND tblman.id_modulo = moduli.id_modulo and tblman.tipo_fw = tipo_fw.tipo_fw and tblman.id_prodotto = prodotti.id_prodotto order by " & ordine_alfabetico), db1, adOpenStatic, adLockReadOnly
    Else
        rs1.Open ("SELECT " & ordine_visualizzazione & " FROM tblman, clienti, moduli, tipo_fw, prodotti where  tblman.id_cliente = clienti.id_cliente AND tblman.id_modulo = moduli.id_modulo and tblman.tipo_fw = tipo_fw.tipo_fw and tblman.id_prodotto = prodotti.id_prodotto order by " & ordine_alfabetico), db1, adOpenStatic, adLockReadOnly
    End If
    rs1.Requery
    MSHFlexGrid1.MergeCells = 0
    
    If rs1.RecordCount > 0 Then
        Set MSHFlexGrid1.DataSource = rs1
        Call Dimensionamento_gliglia
        MSHFlexGrid1.MergeCells = 4
        MSHFlexGrid1.MergeCol(0) = True
        MSHFlexGrid1.MergeCol(1) = True
        MSHFlexGrid1.MergeCol(2) = True
    Else
        MSHFlexGrid1.FixedRows = 1
    End If
    MSHFlexGrid1.Visible = True
    'MSHFlexGrid1.Refresh
    
aa:
    aa = aa
    
    Resume Next
    End Sub

Sub Dimensionamento_gliglia()


For a = 0 To MSHFlexGrid1.Cols - 1
    MSHFlexGrid1.Row = 0
    MSHFlexGrid1.Col = a
    Select Case MSHFlexGrid1.Text
        Case "Cod_Prodotto"
            MSHFlexGrid1.ColWidth(a) = MSHFlexGrid1.Width * 5 / 100
            dist = 0
            For B = 0 To a - 1
                dist = dist + MSHFlexGrid1.ColWidth(B)
            Next
            Label5.Left = dist + MSHFlexGrid1.Left
        Case "Cat_Prod"
            MSHFlexGrid1.ColWidth(a) = MSHFlexGrid1.Width * 6 / 100
        Case "Cliente"
            MSHFlexGrid1.ColWidth(a) = MSHFlexGrid1.Width * 14 / 100
        Case "Cod_Modulo"
            MSHFlexGrid1.ColWidth(a) = MSHFlexGrid1.Width * 10 / 100
        Case "Descrizione_Modulo"
            MSHFlexGrid1.ColWidth(a) = MSHFlexGrid1.Width * 15 / 100
        Case "Cod_Firmware"
            MSHFlexGrid1.ColWidth(a) = MSHFlexGrid1.Width * 7 / 100
            dist = 0
            For B = 0 To a - 1
                dist = dist + MSHFlexGrid1.ColWidth(B)
            Next
            Label6.Left = dist + MSHFlexGrid1.Left
        Case "Cod_Firmware_Field"
            MSHFlexGrid1.ColWidth(a) = MSHFlexGrid1.Width * 7 / 100
            dist = 0
            For B = 0 To a - 1
                dist = dist + MSHFlexGrid1.ColWidth(B)
            Next
            Label7.Left = dist + MSHFlexGrid1.Left

        Case "Tipo_FW"
            MSHFlexGrid1.ColWidth(a) = MSHFlexGrid1.Width * 5 / 100
        Case "Unit_Config"
            MSHFlexGrid1.ColWidth(a) = MSHFlexGrid1.Width * 4 / 100
        Case "Banco"
            MSHFlexGrid1.ColWidth(a) = MSHFlexGrid1.Width * 2 / 100
        Case "Ultima_ECN"
            MSHFlexGrid1.ColWidth(a) = MSHFlexGrid1.Width * 4 / 100
        Case "Setup_Piastra"
            MSHFlexGrid1.ColWidth(a) = MSHFlexGrid1.Width * 5 / 100
            dist = 0
            For B = 0 To a - 1
                dist = dist + MSHFlexGrid1.ColWidth(B)
            Next
            Label8.Left = dist + MSHFlexGrid1.Left
            
End Select
Next
End Sub
Function Ordine_visu(ricerca_per) As String


Select Case ricerca_per
    Case "Cod_Prodotto"
        Ordine_visu = "prodotti.categoria_prodotto AS Cat_Prod, tblman.ID_Prodotto AS Prodotto, clienti.nome_cliente AS Cliente, tblman.id_modulo AS Cod_Modulo, moduli.descrizione AS Descrizione_Modulo, tblman.cod_fw AS Cod_Firmware, tblman.cod_fw_field AS Cod_Firmware_Field, tipo_fw.descrizione AS Tipo_FW, tblman.id_nome_unit_config AS Unit_Config, tblman.Banco, tblman.last_rdm AS Ultima_ECN, tblman.boardsetup AS Setup_Piastra "
            Dim frazione As Double
            frazione = 1 / 13
            Dim i As Integer
            For i = 0 To MSHFlexGrid1.Cols - 1
                MSHFlexGrid1.ColWidth(i) = MSHFlexGrid1.Width * frazione
            Next
            
    Case "Cat_Prod"
        'Ordine_visu = "prodotti.categoria_prodotto as Cat_Prod, clienti.nome_cliente as Cliente, tblman.ID_Prodotto as Prodotto, tblman.id_modulo as Cod_Modulo, moduli.descrizione as Descrizione_Modulo, tblman.cod_fw as Cod_Firmware, tipo_fw.descrizione as Tipo_FW, tblman.last_rdm as Ultima_ECN, tblman.boardsetup as Setup"
        If UCase(Combo2.Text) = "SPAREBOARD" Then
            Ordine_visu = "prodotti.categoria_prodotto as Cat_Prod, clienti.nome_cliente as Cliente, tblman.ID_Prodotto as Prodotto, tblman.id_modulo as Cod_Modulo, moduli.descrizione as Descrizione_Modulo, tblman.cod_fw as Cod_Firmware, tipo_fw.descrizione as Tipo_FW, tblman.last_rdm as Ultima_ECN, tblman.boardsetup as Setup_Piastra"
        Else
            Ordine_visu = "prodotti.categoria_prodotto as Cat_Prod, clienti.nome_cliente as Cliente, tblman.ID_Prodotto as Prodotto, tblman.id_modulo as Cod_Modulo, moduli.descrizione as Descrizione_Modulo, tblman.cod_fw as Cod_Firmware, tblman.cod_fw_field as Cod_Firmware_Field, tipo_fw.descrizione as Tipo_FW, tblman.id_nome_unit_config AS Unit_Config, tblman.Banco, tblman.last_rdm as Ultima_ECN"
        End If
    Case "Cliente"
        Ordine_visu = "clienti.nome_cliente as Cliente, prodotti.categoria_prodotto as Cat_Prod, tblman.ID_Prodotto as Prodotto, tblman.id_modulo as Cod_Modulo, moduli.descrizione as Descrizione_Modulo, tblman.cod_fw as Cod_Firmware, tblman.cod_fw_field as Cod_Firmware_Field, tipo_fw.descrizione as Tipo_FW, tblman.id_nome_unit_config AS Unit_Config, tblman.Banco, tblman.last_rdm as Ultima_ECN"
    
    Case "Cod_Modulo"
        Ordine_visu = "prodotti.categoria_prodotto as Cat_Prod, clienti.nome_cliente as Cliente, tblman.ID_Prodotto as Prodotto, tblman.id_modulo as Cod_Modulo, moduli.descrizione as Descrizione_Modulo, tblman.cod_fw as Cod_Firmware, tblman.cod_fw_field as Cod_Firmware_Field, tipo_fw.descrizione as Tipo_FW, tblman.Banco, tblman.last_rdm as Ultima_ECN"
    
    Case "Descrizione_Modulo"
        Ordine_visu = "prodotti.categoria_prodotto as Cat_Prod, clienti.nome_cliente as Cliente, tblman.ID_Prodotto as Prodotto, tblman.id_modulo as Cod_Modulo, moduli.descrizione as Descrizione_Modulo, tblman.cod_fw as Cod_Firmware, tblman.cod_fw_field as Cod_Firmware_Field, tipo_fw.descrizione as Tipo_FW, tblman.Banco, tblman.last_rdm as Ultima_ECN"
    
    Case "Cod_Firmware", "Cod_Firmware_Field"
        Ordine_visu = "prodotti.categoria_prodotto as Cat_Prod, clienti.nome_cliente as Cliente, tblman.ID_Prodotto as Prodotto, tblman.id_modulo as Cod_Modulo, moduli.descrizione as Descrizione_Modulo, tblman.cod_fw as Cod_Firmware, tblman.cod_fw_field as Cod_Firmware_Field, tipo_fw.descrizione as Tipo_FW, tblman.Banco, tblman.last_rdm as Ultima_ECN"
    
    Case "Nome_Interno"
        Ordine_visu = "prodotti.categoria_prodotto as Cat_Prodotto, clienti.nome_cliente as Cliente, tblman.ID_Prodotto as Prodotto, tblman.id_modulo as Cod_Modulo, moduli.descrizione as Descrizione_Modulo, tblman.cod_fw as Cod_Firmware, tblman.cod_fw_field as Cod_Firmware_Field, tipo_fw.descrizione as Tipo_FW, tblman.Banco, tblman.last_rdm as Ultima_ECN"
    
    Case "Tipo_FW"
        Ordine_visu = "prodotti.categoria_prodotto as Cat_Prod, clienti.nome_cliente as Cliente, tblman.ID_Prodotto as Prodotto, tblman.id_modulo as Cod_Modulo, moduli.descrizione as Descrizione_Modulo, tblman.cod_fw as Cod_Firmware, tblman.cod_fw_field as Cod_Firmware_Field, tipo_fw.descrizione as Tipo_FW, tblman.Banco, tblman.last_rdm as Ultima_ECN"
    
    Case "Banco"
        Ordine_visu = "prodotti.categoria_prodotto as Cat_Prod, clienti.nome_cliente as Cliente, tblman.ID_Prodotto as Prodotto, tblman.id_modulo as Cod_Modulo, moduli.descrizione as Descrizione_Modulo, tblman.cod_fw as Cod_Firmware, tblman.cod_fw_field as Cod_Firmware_Field, tipo_fw.descrizione as Tipo_FW, tblman.Banco, tblman.last_rdm as Ultima_ECN"
    
    Case "Ultima_ECN"
        Ordine_visu = "prodotti.categoria_prodotto as Cat_Prod, clienti.nome_cliente as Cliente, tblman.ID_Prodotto as Prodotto, tblman.id_modulo as Cod_Modulo, moduli.descrizione as Descrizione_Modulo, tblman.cod_fw as Cod_Firmware, tblman.cod_fw_field as Cod_Firmware_Field, tipo_fw.descrizione as Tipo_FW, tblman.Banco, tblman.last_rdm as Ultima_ECN"

End Select

    
End Function

Function Ordine_alfa(ricerca_per)

Select Case ricerca_per
    Case "Cod_Prodotto"
        Ordine_alfa = "prodotti.categoria_prodotto, tblman.ID_Prodotto, clienti.Nome_cliente, tblman.id_modulo, tblman.Tipo_fw, tblman.banco"
    
    Case "Cat_Prod"
        Ordine_alfa = "prodotti.categoria_prodotto, tblman.ID_Prodotto, clienti.Nome_cliente, tblman.id_modulo, tblman.Tipo_fw, tblman.banco"
        
    Case "Cliente"
        Ordine_alfa = "prodotti.categoria_prodotto, tblman.ID_Prodotto, clienti.Nome_cliente, tblman.id_modulo, tblman.Tipo_fw, tblman.banco"
    
    Case "Cod_Modulo"
        Ordine_alfa = "prodotti.categoria_prodotto, tblman.ID_Prodotto, clienti.Nome_cliente, tblman.id_modulo, tblman.Tipo_fw, tblman.banco"
    
    Case "Descrizione_Modulo"
        Ordine_alfa = "prodotti.categoria_prodotto, tblman.ID_Prodotto, clienti.Nome_cliente, tblman.id_modulo, tblman.Tipo_fw, tblman.banco"
    
    Case "Cod_Firmware", "Cod_Firmware_Field"
        Ordine_alfa = "prodotti.categoria_prodotto, tblman.ID_Prodotto, clienti.Nome_cliente, tblman.id_modulo, tblman.Tipo_fw, tblman.banco"
    
    Case "Nome_Interno"
        Ordine_alfa = "prodotti.categoria_prodotto, tblman.ID_Prodotto, clienti.Nome_cliente, tblman.id_modulo, tblman.Tipo_fw, tblman.banco"
    
    Case "Tipo_FW"
        Ordine_alfa = "prodotti.categoria_prodotto, tblman.ID_Prodotto, clienti.Nome_cliente, tblman.id_modulo, tblman.Tipo_fw, tblman.banco"
    
    Case "Banco"
        Ordine_alfa = "prodotti.categoria_prodotto, tblman.ID_Prodotto, clienti.Nome_cliente, tblman.id_modulo, tblman.Tipo_fw, tblman.banco"
    
    Case "Ultima_ECN"
        Ordine_alfa = "prodotti.categoria_prodotto, tblman.ID_Prodotto, clienti.Nome_cliente, tblman.id_modulo, tblman.Tipo_fw, tblman.banco"

End Select

End Function

Function ordinamento(ricerca_per) As String
Select Case ricerca_per
    Case "Cod_Prodotto"
        ordinamento = "tblman.ID_Prodotto, prodotti.categoria_prodotto, clienti.Nome_cliente, tblman.id_modulo, tblman.Tipo_fw, tblman.banco"
    
    Case "Cat_Prod"
        ordinamento = "prodotti.categoria_prodotto, tblman.ID_Prodotto, clienti.Nome_cliente, tblman.id_modulo, tblman.Tipo_fw, tblman.banco"
    
    Case "Cliente"
        ordinamento = "clienti.Nome_cliente, prodotti.categoria_prodotto, tblman.ID_Prodotto, tblman.id_modulo, tblman.Tipo_fw, tblman.banco"
    
    Case "Cod_Modulo"
        ordinamento = "tblman.id_modulo, prodotti.categoria_prodotto, tblman.ID_Prodotto, clienti.Nome_cliente, tblman.id_modulo, tblman.Tipo_fw, tblman.banco"
    
    Case "Descrizione_Modulo"
        ordinamento = "moduli.descrizione, prodotti.categoria_prodotto, tblman.ID_Prodotto, clienti.Nome_cliente,tblman.id_modulo, tblman.Tipo_fw, tblman.banco"
    
    Case "Cod_Firmware"
        ordinamento = "tblman.cod_fw, prodotti.categoria_prodotto, tblman.ID_Prodotto, clienti.Nome_cliente, tblman.id_modulo, tblman.Tipo_fw, tblman.banco"
    
    Case "Cod_Firmware_Field"
        ordinamento = "tblman.cod_fw_field, prodotti.categoria_prodotto, tblman.ID_Prodotto, clienti.Nome_cliente, tblman.id_modulo, tblman.Tipo_fw, tblman.banco"

    Case "Nome_Interno"
        ordinamento = "prodotti.categoria_prodotto, tblman.ID_Prodotto, clienti.Nome_cliente, tblman.id_modulo, tblman.Tipo_fw, tblman.banco"
    
    Case "Tipo_FW"
        ordinamento = "tblman.Tipo_fw, prodotti.categoria_prodotto, tblman.ID_Prodotto, clienti.Nome_cliente, tblman.id_modulo, tblman.banco"
    
    Case "Banco"
        ordinamento = "tblman.banco, prodotti.categoria_prodotto, tblman.ID_Prodotto, clienti.Nome_cliente, tblman.id_modulo, tblman.Tipo_fw"
    
    Case "Ultima_ECN"
        ordinamento = "tblman.last_rdm, prodotti.categoria_prodotto, tblman.ID_Prodotto, clienti.Nome_cliente, tblman.id_modulo, tblman.Tipo_fw, tblman.banco"

End Select

End Function

Private Sub SQL_Click()
Advanced.Visible = True
End Sub

Private Sub Stampa_Click()
Set Griglia_da_stampare = MSHFlexGrid1
frmPRINT.Visible = True

End Sub

Sub R_advanced()


End Sub

Private Sub UM_Click()
Nuova_versione.Visible = True
End Sub

Private Sub ver_Click()
frmAbout.Visible = True
End Sub
