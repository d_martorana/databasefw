VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form frmCC3R 
   Caption         =   "CC3R CONFIGURATION"
   ClientHeight    =   6960
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13140
   LinkTopic       =   "Form2"
   ScaleHeight     =   6960
   ScaleWidth      =   13140
   StartUpPosition =   3  'Windows Default
   Begin VB.ComboBox cboCliente 
      Height          =   315
      Left            =   1440
      TabIndex        =   4
      Text            =   "cboCliente"
      Top             =   480
      Width           =   4815
   End
   Begin VB.ComboBox cboProdotto 
      Height          =   315
      Left            =   1440
      TabIndex        =   3
      Text            =   "cboProdotto"
      Top             =   120
      Width           =   4815
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid grdCC3R 
      Height          =   4215
      Left            =   120
      TabIndex        =   0
      Top             =   2160
      Width           =   10935
      _ExtentX        =   19288
      _ExtentY        =   7435
      _Version        =   393216
      FixedCols       =   0
      Appearance      =   0
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "Cliente"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   1215
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Product code"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "frmCC3R"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim databaseFileName As String
Dim myDataBase As ADODB.Connection
Dim myRecordSet As ADODB.Recordset
Dim myRsClienti As ADODB.Recordset


Private Sub cboCliente_Click()
    Set myRsClienti = New ADODB.Recordset
    Set myRecordSet = New ADODB.Recordset
    Dim cliente As Integer
    'cboCliente.Clear
    myDataBase.Open "Provider =Microsoft.Jet.OLEDB.4.0;Data Source=" & databaseFileName & ";"
    myRsClienti.Open ("SELECT id_cliente FROM clienti WHERE nome_cliente='" & cboCliente.Text & "'"), myDataBase, adOpenStatic, adLockReadOnly
    cliente = myRsClienti("id_cliente")
    myRecordSet.Open ("SELECT * FROM tblCC3R WHERE id_prodotto='" & cboProdotto.Text & "' AND id_cliente=" & cliente), myDataBase, adOpenStatic, adLockReadOnly
    Set grdCC3R.DataSource = myRecordSet
    grdCC3R.Refresh
    myDataBase.Close
End Sub

Private Sub cboProdotto_Click()
    Set myRsClienti = New ADODB.Recordset
    Set myRecordSet = New ADODB.Recordset
    cboCliente.Clear
    myDataBase.Open "Provider =Microsoft.Jet.OLEDB.4.0;Data Source=" & databaseFileName & ";"
    myRecordSet.Open ("SELECT * FROM tblCC3R WHERE id_prodotto='" & cboProdotto.Text & "'"), myDataBase, adOpenStatic, adLockReadOnly
    
    Dim i As Integer
    For i = 0 To myRecordSet.RecordCount - 1
        myRsClienti.Open ("SELECT * FROM clienti WHERE id_cliente=" & myRecordSet("id_cliente")), myDataBase, adOpenStatic, adLockReadOnly
        cboCliente.AddItem myRsClienti("nome_cliente")
        myRsClienti.Close
        myRecordSet.MoveNext
    Next
    myDataBase.Close
End Sub

Private Sub Form_Load()
    databaseFileName = Read_Set("Server", "databasePath")
    databaseFileName = Left(databaseFileName, Len(databaseFileName) - 1)
        
    Set myDataBase = New ADODB.Connection
    Set myRecordSet = New ADODB.Recordset
    myDataBase.Open "Provider =Microsoft.Jet.OLEDB.4.0;Data Source=" & databaseFileName & ";"
    myRecordSet.Open ("SELECT DISTINCT * FROM tblCC3R"), myDataBase, adOpenStatic, adLockReadOnly
    Set grdCC3R.DataSource = myRecordSet
    
    cboProdotto.Clear
    cboCliente.Clear
    Dim i As Integer
    For i = 0 To myRecordSet.EOF
        cboProdotto.AddItem myRecordSet("id_prodotto")
        myRecordSet.MoveNext
    Next
'    For i = 0 To myRecordSet.RecordCount - 1
'        For z = 0 To 9
'            cboValuta(z).AddItem myRecordSet("iso_code")
'        Next z
'        myRecordSet.MoveNext
'    Next i
    myRecordSet.Close
    myDataBase.Close
    Me.WindowState = 2
End Sub

Private Sub Form_Resize()
    grdCC3R.Width = Me.Width - 360
    grdCC3R.Height = (Me.Height - grdCC3R.Top) - 600
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Form1.Enabled = True
End Sub

