Attribute VB_Name = "Parametri"

Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long

Public Function Write_Set(Filename As String, Sezione As String, Parametro As String, Valore) As Integer


Dim Mode As String


Mode = "WRITE"
tmpSecname = "SEZIONE"

aa = ReadWriteINI(Mode, Sezione, Parametro, Filename, Valore)

End Function

Public Function Read_Ini(Filename As String, Sezione As String, Parametro As String) As String

    
    Dim Mode As String
    Dim tmpKeyValue
    
    
    Mode = "GET"
    
    aa = ReadWriteINI(Mode, Sezione, Parametro, Filename, tmpKeyValue)
    Read_Ini = tmpKeyValue

End Function


Public Function Read_Set(Sezione As String, Parametro As String) As String

Dim Filename As String
Dim Mode As String
Dim tmpKeyValue

Filename = WorkDir + "SetupNR.ini"
Mode = "GET"

aa = ReadWriteINI(Mode, Sezione, Parametro, Filename, tmpKeyValue)
Read_Set = tmpKeyValue

End Function



Public Function ReadWriteINI(Mode As String, tmpSecname As String, tmpKeyname As String, Filename As String, Optional tmpKeyValue) As String
On Error GoTo ReadWriteINIError

    Dim tmpString As String

    ReadWriteINI = "OK"

    If Len(Mode) = 0 Then
        ReadWriteINI = "ERROR MODE"
        Exit Function
    End If

    If Len(tmpSecname) = 0 Then
        ReadWriteINI = "ERROR Secname"
        Exit Function
    End If

    If Len(tmpKeyname) = 0 Then
        ReadWriteINI = "ERROR Keyname"
        Exit Function
    End If

    If UCase(Mode) = "WRITE" Then
            If Len(tmpKeyValue) = 0 Then
            ReadWriteINI = "ERROR KeyValue"
            Exit Function
        Else
            Dim secname
            Dim KeyName As String
            Dim keyvalue As String
            
            secname = tmpSecname
            KeyName = tmpKeyname
            keyvalue = tmpKeyValue
            anInt = WritePrivateProfileString(secname, KeyName, keyvalue, Filename)
        End If
End If
    If UCase(Mode) = "GET" Then
            secname = tmpSecname
            KeyName = tmpKeyname
            defaultkey = "Failed"
            keyvalue = String$(500, 32)
            anInt = GetPrivateProfileString(secname, KeyName, defaultkey, keyvalue, Len(keyvalue), Filename)
            If Left(keyvalue, 6) = "Failed" Then
                tmpString = keyvalue
                tmpString = RTrim(tmpString)
                tmpString = Left(tmpString, Len(tmpString) - 1)
            End If
            tmpKeyValue = Trim(keyvalue)
    End If
    Exit Function
   
ReadWriteINIError:
    MsgBox error
    'Stop

End Function


