VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form frmCustomization 
   Caption         =   "TABELLA CUSTOMIZZAZIONE"
   ClientHeight    =   8925
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   18495
   LinkTopic       =   "Form2"
   ScaleHeight     =   8925
   ScaleWidth      =   18495
   StartUpPosition =   3  'Windows Default
   Begin VB.ComboBox cboCliente 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1440
      TabIndex        =   2
      Top             =   600
      Width           =   5055
   End
   Begin VB.ComboBox cboProdotto 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1440
      TabIndex        =   1
      Top             =   240
      Width           =   2055
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid grdCustomizzazione 
      CausesValidation=   0   'False
      Height          =   5295
      Left            =   120
      TabIndex        =   0
      Top             =   1200
      Width           =   17040
      _ExtentX        =   30057
      _ExtentY        =   9340
      _Version        =   393216
      Cols            =   1
      FixedCols       =   0
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      FocusRect       =   0
      GridLines       =   2
      GridLinesUnpopulated=   2
      MergeCells      =   4
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   1
   End
   Begin VB.Label Label2 
      Caption         =   "Product code"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   240
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Client"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   1215
   End
End
Attribute VB_Name = "frmCustomization"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim dbName As String
Dim myDb As ADODB.Connection
Dim myRecordSet As ADODB.Recordset
Dim myRsClienti As ADODB.Recordset


Private Sub cboCliente_Click()
    Set myRsClienti = New ADODB.Recordset
    Set myRecordSet = New ADODB.Recordset
    Dim cliente As Integer
    'cboCliente.Clear
    myDb.Open "Provider =Microsoft.Jet.OLEDB.4.0;Data Source=" & dbName & ";"
    myRsClienti.Open ("SELECT id_cliente FROM clienti WHERE nome_cliente='" & cboCliente.Text & "'"), myDb, adOpenStatic, adLockReadOnly
    cliente = myRsClienti("id_cliente")
    myRecordSet.Open ("SELECT * FROM tblcustomization WHERE id_prodotto='" & cboProdotto.Text & "' AND id_cliente=" & cliente), myDb, adOpenStatic, adLockReadOnly
    Set grdCustomizzazione.DataSource = myRecordSet
    grdCustomizzazione.Refresh
    myDb.Close
End Sub

Private Sub cboProdotto_Click()
    cboProdotto.Enabled = False

    grdCustomizzazione.Clear
    cboCliente.Clear
    myDb.Open "Provider = Microsoft.Jet.OLEDB.4.0; Data Source=" & dbName & ";"
    myRecordSet.Open ("SELECT * FROM tblcustomization WHERE id_prodotto='" & cboProdotto.Text & "'"), myDb, adOpenStatic, adLockReadOnly
    msDELAY (0.5)
    myRecordSet.Requery
    Set grdCustomizzazione.DataSource = myRecordSet
    Set myRsClienti = New ADODB.Recordset
    myRecordSet.MoveFirst
    Do Until myRecordSet.EOF
        myRsClienti.Open ("SELECT nome_cliente FROM clienti WHERE id_cliente=" & myRecordSet("id_cliente")), myDb, adOpenStatic, adLockReadOnly
        cboCliente.AddItem myRsClienti("nome_cliente").Value
        myRecordSet.MoveNext
    Loop
    myRecordSet.Close
    myDb.Close
    cboProdotto.Enabled = True
End Sub

Private Sub Form_Load()
    cboCliente.Clear
    cboProdotto.Clear
    
    
    dbName = Read_Set("Server", "databasePath")
    dbName = Left(dbName, Len(dbName) - 1)
    
    Set myDb = New ADODB.Connection
    Set myRecordSet = New ADODB.Recordset
    myDb.Open "Provider = Microsoft.Jet.OLEDB.4.0; Data Source=" & dbName & ";"
    myRecordSet.CursorLocation = adUseClient
    myRecordSet.Open ("SELECT * FROM tblcustomization"), myDb, adOpenStatic, adLockReadOnly
    msDELAY (0.5)
    myRecordSet.Requery
    
    grdCustomizzazione.Cols = 28
    Set grdCustomizzazione.DataSource = myRecordSet
    'dimensionamento griglia
        
    grdCustomizzazione.FontBand(0).Bold = True
    grdCustomizzazione.Font.Size = 9
    grdCustomizzazione.Font.Bold = False

    myRecordSet.MoveFirst
    Do Until myRecordSet.EOF
        cboProdotto.AddItem myRecordSet("id_prodotto").Value
        myRecordSet.MoveNext
    Loop
    myRecordSet.Close
    myDb.Close
    frmCustomization.WindowState = 2
'    grdCustomizzazione.MergeCells = flexMergeFree
'    grdCustomizzazione.MergeCol(0) = True
'    grdCustomizzazione.MergeCol(1) = True
'    grdCustomizzazione.MergeCol(2) = True
'    grdCustomizzazione.MergeCol(3) = True
'    grdCustomizzazione.MergeCol(4) = True
    
End Sub


Private Sub Form_Resize()
    If frmCustomization.Height > 1800 And frmCustomization.Width > 250 Then
        grdCustomizzazione.Width = frmCustomization.Width - 250
        grdCustomizzazione.Height = frmCustomization.Height - 1800
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Form1.Enabled = True
End Sub
