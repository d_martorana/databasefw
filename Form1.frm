VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H006A1161&
   Caption         =   "DATABASE_FW"
   ClientHeight    =   9555
   ClientLeft      =   165
   ClientTop       =   855
   ClientWidth     =   14340
   LinkTopic       =   "Form1"
   ScaleHeight     =   9555
   ScaleWidth      =   14340
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdCC3R 
      Caption         =   "CC3R PRODUCTS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   9720
      MaskColor       =   &H00FFC0FF&
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   6360
      Width           =   4000
   End
   Begin VB.CommandButton cmdCustomizzazione 
      Caption         =   "END USER CUSTOMIZATION"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   2880
      TabIndex        =   5
      Top             =   5640
      Width           =   4000
   End
   Begin VB.CommandButton btnRicercaCDF 
      Caption         =   "CDF SEARCH by CURRENCY"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   3720
      TabIndex        =   4
      Top             =   7800
      Width           =   7455
   End
   Begin VB.CommandButton Command2 
      BackColor       =   &H00FFFFFF&
      Caption         =   "RECYCLERS && PCBA SET-UP"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   9600
      TabIndex        =   2
      Top             =   3240
      Width           =   4000
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Modifica dati"
      Height          =   1860
      Left            =   9240
      TabIndex        =   1
      Top             =   315
      Visible         =   0   'False
      Width           =   2625
   End
   Begin VB.CommandButton Command1 
      Caption         =   "OFFICIAL FILES ARCHIVE"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   3120
      TabIndex        =   0
      Top             =   3600
      Width           =   4000
   End
   Begin VB.Image Image3 
      Height          =   630
      Left            =   2400
      Picture         =   "Form1.frx":0000
      Top             =   360
      Width           =   2325
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H006A1161&
      Caption         =   "FIRMWARE and CURRENCY DATA FILES DATABASE"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   435
      Left            =   2340
      TabIndex        =   3
      Top             =   2175
      Width           =   9165
   End
   Begin VB.Menu Versione 
      Caption         =   "Help"
      Begin VB.Menu Ver 
         Caption         =   "Version"
      End
      Begin VB.Menu Ultima 
         Caption         =   "Last modify"
      End
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub btnRicercaCDF_Click()
    frmCDF.Show
End Sub

Private Sub cmdCC3R_Click()
    Form1.Enabled = False
    frmCC3R.Visible = True
End Sub

Private Sub cmdCustomizzazione_Click()
    Form1.Enabled = False
    frmCustomization.Visible = True
End Sub

Private Sub Command1_Click()
    Form1.Enabled = False
    Dettaglio_FW.Visible = True
End Sub

Private Sub Command2_Click()
    Form1.Enabled = False
    Ricerca.Visible = True
End Sub


Private Sub Form_Resize()
    Dim spaziatura, hBtn, hLbl As Integer
    Scala = Me.Height / 14400
    hBtn = 1245 * Scala
    hLbl = 435 * Scala
    spaziatura = 200 * Scala
    
    Image3.Height = 630
    Command1.Height = hBtn
    Command2.Height = hBtn
    btnRicercaCDF.Height = hBtn
    cmdCC3R.Height = hBtn
    cmdCustomizzazione.Height = hBtn
    Label1.Height = 435
    
    btnRicercaCDF.Width = Command1.Width + Command2.Width + spaziatura
    Image3.Width = 2325
    
    Command1.Left = (Me.Width - Command1.Width) / 2 - Command1.Width / 2 - spaziatura / 2
    Command2.Left = (Me.Width - Command2.Width) / 2 + Command2.Width / 2 + spaziatura / 2
    cmdCustomizzazione.Left = Command1.Left
    cmdCC3R.Left = Command2.Left
    btnRicercaCDF.Left = (Me.Width - btnRicercaCDF.Width) / 2  '- (btnRicercaCDF.Width / 2)
    Label1.Left = (Me.Width - Label1.Width) / 2
    Image3.Left = spaziatura * 2 'Me.Width - spaziatura * 2 - Image3.Width
    
    Label1.Top = (Me.Height - (hBtn * 3 + Label1.Height + 3 * spaziatura)) / 2 - 600
    Command1.Top = Label1.Top + Label1.Height * 2 + spaziatura
    Command2.Top = Label1.Top + Label1.Height * 2 + spaziatura
    cmdCustomizzazione.Top = Command1.Top + Command1.Height + spaziatura
    cmdCC3R.Top = Command2.Top + Command2.Height + spaziatura
    btnRicercaCDF.Top = cmdCustomizzazione.Top + cmdCustomizzazione.Height + spaziatura
    Image3.Top = 500 * Scala
    
End Sub

Private Sub Form_Activate()
    Dim obj As Object
    If disposizione_form1 = False Then
        Call Form_Resize
        aa = Dir("c:\database_fw\database_fw.ini")
        If aa = "" Then
            MkDir "c:\database_fw\"
            aa = Write_Set("c:\database_fw\database_fw.ini", "Tips", "Version " & App.Major & "." & App.Minor & "." & App.Revision, "0")
        End If
        If Mid(Read_Ini("c:\database_fw\database_fw.ini", "Tips", "Version " & App.Major & "." & App.Minor & "." & App.Revision), 1, 6) = "Failed" Then
            Call Write_Set("c:\database_fw\database_fw.ini", "Tips", "Version " & App.Major & "." & App.Minor & "." & App.Revision, "0")
            Nuova_versione.Visible = True
        Else
            If Read_Ini("c:\database_fw\database_fw.ini", "Tips", "Version " & App.Major & "." & App.Minor & "." & App.Revision) = 0 Then
                Nuova_versione.Visible = True
            End If
        End If
        disposizione_form1 = True
    End If
    
    'MsgBox Screen.Width & " - " & Me.Width
End Sub

Private Sub Form_Load()

    WorkDir = App.Path
    
    If Mid$(WorkDir, Len(WorkDir), 1) <> "\" Then
        WorkDir = WorkDir + "\"
    End If
    
    

End Sub



Private Sub Form_Terminate()
Unload Form1
Unload Ricerca
Unload Dettaglio_FW

End
End Sub

Private Sub Form_Unload(Cancel As Integer)
Unload Form1
Unload Ricerca
Unload Dettaglio_FW
End
End Sub

Private Sub ultima_Click()
Nuova_versione.Visible = True
End Sub

Private Sub ver_Click()
frmAbout.Visible = True
End Sub

